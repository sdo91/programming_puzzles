#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUTS = [
    """
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
    """, """
..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
..........
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    8,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    4,
    None,
]

REAL_OUTPUTS = [
    6870,
    287
]


class PipeMaze(Grid2D):
    """
    The pipes are arranged in a two-dimensional grid of tiles:
        | is a vertical pipe connecting north and south.
        - is a horizontal pipe connecting east and west.
        L is a 90-degree bend connecting north and east.
        J is a 90-degree bend connecting north and west.
        7 is a 90-degree bend connecting south and west.
        F is a 90-degree bend connecting south and east.
        . is ground; there is no pipe in this tile.
        S is the starting position of the animal;
            there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
    """

    tiles_to_dirs = {
        '|': set('NS'),
        '-': set('EW'),
        'L': set('NE'),
        'J': set('NW'),
        '7': set('SW'),
        'F': set('SE'),
    }

    def __init__(self, text):
        super().__init__(text)

    def is_b_connected(self, coord_a, coord_b):
        """
        return True if tile B connects to A
        """
        tile_b = self.get_tuple(coord_b)
        if tile_b not in self.tiles_to_dirs:
            return False  # B is not a pipe tile

        # get direction from B to A
        dir_b_to_a = self.get_direction(coord_b, coord_a)

        return dir_b_to_a in self.tiles_to_dirs[tile_b]

    def get_next_coord(self, coord_a, coord_b):
        # get all direction from B
        tile_b = self.get_tuple(coord_b)
        directions_from_b = tuple(self.tiles_to_dirs[tile_b])

        # get direction from B to A
        dir_b_to_a = self.get_direction(coord_b, coord_a)

        # choose the other direction
        if directions_from_b[0] == dir_b_to_a:
            next_direction = directions_from_b[1]
        else:
            next_direction = directions_from_b[0]

        # get the next coord
        next_coord = self.get_coord_direction(coord_b, next_direction)
        return next_coord

    def is_inside(self, coord, path_coords):
        """
        use even odd rule
        go north
        """
        if coord in path_coords:
            return False  # coords on the border are not inside

        num_dash = 0
        num_7L = 0
        num_JF = 0

        # go north until we get out of bounds
        current_coord = coord
        while True:
            current_coord = self.get_coord_direction(current_coord, 'N')
            if self.is_out_of_bounds(current_coord):
                break

            # crossings can only happen on path coords
            if current_coord not in path_coords:
                continue

            tile = self.get_tuple(current_coord)

            if tile == '-':
                num_dash += 1
            elif tile == '7' or tile == 'L':
                num_7L += 1
            elif tile == 'J' or tile == 'F':
                num_JF += 1

        num_crossings = num_dash + num_7L / 2 - num_JF / 2

        is_inside = (num_crossings % 2) == 1
        return is_inside


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = aoc_util.lines(self.text)

        self.maze = PipeMaze(text)
        self.maze.show()
        self.start_coord = self.maze.find_only('S')

        AocLogger.log(str(self))

        # p2
        self.path_coords = {self.start_coord}

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        algo:
            find start
            move to a connected tile
            while not at S:
                move to next connected
        """

        pipe_maze = self.maze

        # find path to 2nd pipe
        for coord in pipe_maze.get_adjacent_coords(self.start_coord):
            if pipe_maze.is_b_connected(self.start_coord, coord):
                # AocLogger.log(coord)
                coord_b = coord
                break

        path = 'S'
        coord_a = self.start_coord
        while coord_b != self.start_coord:
            tile_b = pipe_maze.get_tuple(coord_b)
            path += tile_b
            AocLogger.log(path)
            self.path_coords.add(coord_b)  # p2

            # advance coords
            coord_a, coord_b = coord_b, pipe_maze.get_next_coord(coord_a, coord_b)

        return len(path) // 2

    def p2(self):
        self.p1()
        pipe_maze = self.maze

        # todo: replace the S with the actual tile?
        # pipe_maze.set_tuple(self.start_coord, 'F')

        # test a specific coord
        # test = pipe_maze.is_inside((2, 6), self.path_coords)

        result = 0
        for coord in pipe_maze.coords():
            if coord in self.path_coords:
                pass

            # check if inside
            if pipe_maze.is_inside(coord, self.path_coords):
                result += 1

        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
