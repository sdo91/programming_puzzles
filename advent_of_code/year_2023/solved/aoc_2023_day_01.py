#!/usr/bin/env python3
from typing import List


def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import re
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
    """, """
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
    """, """

    """
]

TEST_OUTPUT_1 = [
    142,
    None,
    None,
]

TEST_OUTPUT_2 = [
    None,
    281,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            54990,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            54473,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True

        assert Solver.get_2_digits('eightwo') == [8, 2]

        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text, 1)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text, 2)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for line in self.lines:
            digits = aoc_util.digits(line)
            result += self.calc_calibration_value(digits)
        return result

    def p2(self):
        """
        54489 is too high
        """
        result = 0
        for line in self.lines:
            digits = self.get_2_digits(line)
            result += self.calc_calibration_value(digits)
        return result

    @classmethod
    def calc_calibration_value(cls, digits: List[int]):
        return 10 * digits[0] + digits[-1]

    @classmethod
    def to_digit(cls, text: str):
        mapping = {
            'one': 1,
            'two': 2,
            'three': 3,
            'four': 4,
            'five': 5,
            'six': 6,
            'seven': 7,
            'eight': 8,
            'nine': 9
        }
        if text in mapping:
            return mapping[text]
        else:
            return int(text)

    @classmethod
    def get_2_digits(cls, text: str):
        """
        handle: eightwothree
        """
        left = cls.find(text, 'left')
        right = cls.find(text, 'right')
        return [left, right]

    @classmethod
    def find(cls, text: str, side: str):
        regex = '1|2|3|4|5|6|7|8|9|one|two|three|four|five|six|seven|eight|nine'
        if side.startswith('r'):
            regex = regex[::-1]
            text = text[::-1]
        matches = re.findall(regex, text)
        result = matches[0]
        if side.startswith('r'):
            result = result[::-1]
        return cls.to_digit(result)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
