#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
import string

from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUTS = [
    """
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    4361,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    467835,
    None,
    None,
]

REAL_OUTPUTS = [
    546563,
    None
]


class Schematic(Grid2D):
    def __init__(self, text=''):
        super().__init__(text, default='.')

        self.symbols = self.unique_values - set(string.digits + '.')

    def get_part_num(self, coord):
        # get number
        if not self.is_start_of_number(coord):
            return 0
        digits = self.read_digits(coord)
        AocLogger.log('found: {}'.format([coord, digits]))

        # get block around number
        top_left = Grid2D.get_coord_nw(coord)
        bottem_right = Grid2D.adjust_coord(coord, dx=len(digits), dy=1)
        self.show_from(top_left, bottem_right)

        # find symbol
        has_symbol = self.count_values_in_block(top_left, bottem_right, self.symbols)
        if has_symbol:
            return int(digits)
        else:
            return 0

    def is_start_of_number(self, coord):
        if self.is_digit(coord):
            left_coord = Grid2D.get_coord_west(coord)
            if not self.is_digit(left_coord):
                return True
        return False


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.schematic = Schematic(text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        In this schematic, two numbers are not part numbers because they are not adjacent to a symbol:
        114 (top right) and 58 (middle right).
        Every other number is adjacent to a symbol and so is a part number;
        their sum is 4361.

        Of course, the actual engine schematic is much larger.
        What is the sum of all the part numbers in the engine schematic?
        """
        result = 0

        grid = self.schematic
        for y in grid.rows():
            for x in grid.cols():
                coord = (x, y)
                result += grid.get_part_num(coord)

        return result

    def p2(self):
        """
        The missing part wasn't the only issue - one of the gears in the engine is wrong.
        A gear is any * symbol that is adjacent to exactly two part numbers.
        Its gear ratio is the result of multiplying those two numbers together.

        This time, you need to find the gear ratio of every gear and add them all up
        so that the engineer can figure out which gear needs to be replaced.
        """
        result = 0

        grid = self.schematic
        gear_coords = grid.find_all('*')

        for gear_coord in gear_coords:
            adjacent = grid.get_adjacent_coords(gear_coord, include_diagonal=True)
            adj_parts = set()
            for adj in adjacent:
                digits = grid.read_digits(adj)
                if digits:
                    adj_parts.add(int(digits))

            if len(adj_parts) == 2:
                adj_parts = list(adj_parts)
                result += adj_parts[0] * adj_parts[1]

        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
