#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    13,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    30,
    None,
    None,
]

REAL_OUTPUTS = [
    25651,
    19499881
]


class Card(object):

    def __init__(self, text: str):
        self.text = text
        self.id = aoc_util.ints(text)[0]

        tokens = text.split(':')
        text = tokens[1].strip()
        tokens = text.split('|')
        self.winners = set(aoc_util.ints(tokens[0]))
        self.picks = aoc_util.ints(tokens[1])

        self.num_copies = 1

    def calc_score(self):
        power = self.calc_num_matches()
        return (2 ** power) // 2

    def calc_num_matches(self):
        num_matches = 0
        for pick in self.picks:
            if pick in self.winners:
                num_matches += 1
        return num_matches


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = aoc_util.lines(self.text)

        self.cards = {}
        for line in self.lines:
            card = Card(line)
            self.cards[card.id] = card

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        for line in self.lines:
            card = Card(line)
            result += card.calc_score()

        return result

    def p2(self):
        result = 0

        for x in range(1, len(self.cards) + 1):
            card = self.cards[x]
            num_matches = card.calc_num_matches()
            start_index = x + 1
            end_index = start_index + num_matches
            for copy_index in range(start_index, end_index):
                self.cards[copy_index].num_copies += card.num_copies

            result += card.num_copies

        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
