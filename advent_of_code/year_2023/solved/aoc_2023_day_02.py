#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    (1 + 2 + 5),
    None,
    None,
]

TEST_OUTPUT_2 = [
    2286,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            2600,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            86036,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Game(object):
    NUM_R = 12
    NUM_G = 13
    NUM_B = 14

    def __init__(self, line: str):
        """
        12 red cubes, 13 green cubes, and 14 blue cubes.

        Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        """
        self.text = line

        game_text, moves_text = self.text.split(':')

        self.game_id = aoc_util.ints(game_text)[0]

        self.moves = aoc_util.split_and_strip_each(moves_text, ';')

    def is_possible(self):
        for move in self.moves:
            colors = aoc_util.split_and_strip_each(move, ',')

            for color in colors:
                number = aoc_util.ints(color)[0]

                if 'red' in color and number > self.NUM_R:
                    return False
                if 'green' in color and number > self.NUM_G:
                    return False
                if 'blue' in color and number > self.NUM_B:
                    return False

        return True

    def calc_power(self):
        """
        The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together. The power
        of the minimum set of cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630, and 36, respectively. Adding up
        these five powers produces the sum 2286.
        """
        min_r = 0
        min_g = 0
        min_b = 0

        for move in self.moves:
            colors = aoc_util.split_and_strip_each(move, ',')

            for color in colors:
                number = aoc_util.ints(color)[0]

                if 'red' in color:
                    min_r = max(min_r, number)
                if 'green' in color:
                    min_g = max(min_g, number)
                if 'blue' in color:
                    min_b = max(min_b, number)

        return min_r * min_g * min_b


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        self.games = []
        for line in self.lines:
            self.games.append(Game(line))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        Determine which games would have been possible if the bag had been loaded with only
        12 red cubes, 13 green cubes, and 14 blue cubes.
        What is the sum of the IDs of those games?
        """
        result = 0
        for game in self.games:
            if game.is_possible():
                result += game.game_id
        return result

    def p2(self):
        result = 0
        for game in self.games:
            result += game.calc_power()
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
