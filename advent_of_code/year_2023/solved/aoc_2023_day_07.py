#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from collections import defaultdict

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    6440,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    5905,
    None,
    None,
]

REAL_OUTPUTS = [
    252295678,
    250577259
]


class CamelHand(object):
    part = 1

    def __init__(self, text: str):
        self.text = text
        tokens = text.split()
        if len(tokens) == 1:
            tokens.append('0')
        self.bid = int(tokens[1])

        cards = tokens[0]
        self.cards_by_count = self.gen_cards_by_count(cards)

        if self.part == 1:
            self.hand_type = self.calc_type_p1()
        else:
            self.hand_type = self.calc_type_p2(cards)

        # convert to hex for sorting
        self.hex_cards = self.convert_to_hex(cards)

    @classmethod
    def gen_cards_by_count(cls, cards):
        if cls.part == 2:
            cards = cards.replace('J', '')
        counts_by_card = aoc_util.count(cards)
        cards_by_count = defaultdict(set)
        for card, count in counts_by_card.items():
            cards_by_count[count].add(card)
        return cards_by_count

    @classmethod
    def convert_to_hex(cls, cards):
        cards = cards.replace('T', 'a')
        if cls.part == 1:
            cards = cards.replace('J', 'b')
        else:
            cards = cards.replace('J', '1')
        cards = cards.replace('Q', 'c')
        cards = cards.replace('K', 'd')
        cards = cards.replace('A', 'e')
        return cards

    def __repr__(self):
        return '{}, hand_type={}'.format(self.text, self.hand_type)

    def calc_type_p1(self):
        if self.get_num_of_count(5):
            return 5
        elif self.get_num_of_count(4):
            return 4
        elif self.get_num_of_count(3) and self.get_num_of_count(2):
            return 3.5  # full house
        elif self.get_num_of_count(3):
            return 3
        elif self.get_num_of_count(2) == 2:
            return 2
        elif self.get_num_of_count(2):
            return 1
        else:
            return 0

    def get_num_of_count(self, count):
        return len(self.cards_by_count[count])

    def __lt__(self, other):
        if self.hand_type == other.hand_type:
            return self.hex_cards < other.hex_cards
        return self.hand_type < other.hand_type

    def calc_type_p2(self, cards: str):
        # calc 2 highest counts
        counts = list(self.cards_by_count.keys())
        counts.sort(reverse=True)
        try:
            first = counts[0]
        except IndexError:
            first = 0
        if self.get_num_of_count(first) > 1:
            second = first
        else:
            try:
                second = counts[1]
            except IndexError:
                second = 0

        # add jokers
        num_jokers = aoc_util.count(cards)['J']
        first += num_jokers

        # calc hand_type
        if first == 5:
            hand_type = 5
        elif first == 4:
            hand_type = 4
        elif first == 3 and second == 2:
            hand_type = 3.5  # full house
        elif first == 3:
            hand_type = 3
        elif first == 2 and second == 2:
            hand_type = 2
        elif first == 2:
            hand_type = 1
        else:
            hand_type = 0
        return hand_type


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        CamelHand.part = part
        self.hands = []
        for line in aoc_util.lines(self.text):
            self.hands.append(CamelHand(line))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    @classmethod
    def run_tests(cls):
        # CamelHand.part = 2
        # CamelHand('T55J5')

        lines = [
            '32T3K 765',
            'T55J5 684',
            'KK677 28',
            'KTJJT 220',
            'QQQJA 483',
        ]
        cls.sort_hands(lines, 2)

        lines = [
            'AAAAA',
            'AA8AA',
            '23332',
            'TTT98',
            '23432',
            'A23A4',
            '23456',
        ]
        cls.sort_hands(lines, 1)

    @classmethod
    def sort_hands(cls, lines, part):
        CamelHand.part = part
        print()
        print('sort hands for part {}'.format(part))
        hands = []
        for line in lines:
            hands.append(CamelHand(line))
        hands.sort()
        for hand in hands:
            print(hand)

    def p1(self):
        return self.calc_winnings()

    def calc_winnings(self):
        self.hands.sort()

        result = 0
        AocLogger.log('\nhands (part {}):'.format(self.part))
        for i, hand in enumerate(self.hands):
            AocLogger.log(hand)
            rank = i + 1
            score = rank * hand.bid
            result += score
        return result

    def p2(self):
        return self.calc_winnings()


if __name__ == '__main__':
    Solver.run_tests()
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
