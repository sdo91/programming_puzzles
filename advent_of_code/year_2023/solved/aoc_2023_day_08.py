#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
import numpy
from collections import defaultdict

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner

### CONSTANTS ###
TEST_INPUTS = [
    """
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
    """, """
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
    """, """
LR

ooA = (ooB, XXX)
ooB = (XXX, ooZ)
ooZ = (ooB, XXX)
ttA = (ttB, XXX)
ttB = (ttC, ttC)
ttC = (ttZ, ttZ)
ttZ = (ttB, ttB)
XXX = (XXX, XXX)
    """
]

TEST_OUTPUTS_P1 = [
    2,
    6,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    6,
]

REAL_OUTPUTS = [
    11911,
    10151663816849
]


class NetworkNode(object):

    def __init__(self, text: str):
        self.key, self.left, self.right = aoc_util.words(text)

    def __repr__(self):
        return '{} -> {}, {}'.format(self.key, self.left, self.right)


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        tokens = self.text.split('\n\n')

        self.instructions = tokens[0]

        self.lines = aoc_util.lines(tokens[1])
        self.network = {}  # type: dict[str, NetworkNode]
        for line in self.lines:
            node = NetworkNode(line)
            self.network[node.key] = node

        self.cycle_tracker = defaultdict(list)

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        next_addr = 'AAA'
        is_done = False
        while not is_done:
            for c in self.instructions:
                current_node = self.network[next_addr]
                result += 1
                if c == 'L':
                    next_addr = current_node.left
                else:
                    next_addr = current_node.right
                if next_addr == 'ZZZ':
                    is_done = True
                    break

        return result

    def p2(self):
        result = 0

        # get starting nodes
        current_nodes = []
        for addr in self.network.keys():
            if addr.endswith('A'):
                n = self.network[addr]
                current_nodes.append(n)

                i = len(self.cycle_tracker)
                self.cycle_tracker[i] = [0]

        is_done = False
        while not is_done:
            for c in self.instructions:
                current_nodes = self.advance_all(current_nodes, c)
                result += 1
                is_done = self.is_p2_done(current_nodes, result)
                if is_done:
                    break

        if is_done == 'cycle':
            cycles = []
            for i, cycle_list in self.cycle_tracker.items():
                delta = cycle_list[-1] - cycle_list[-2]
                delta2 = cycle_list[-2] - cycle_list[-3]
                assert delta == delta2
                cycles.append(delta)
            result = numpy.lcm.reduce(cycles)

        return result

    def advance_all(self, current_nodes, instruction):
        next_nodes = []
        for node in current_nodes:
            if instruction == 'L':
                next_addr = node.left
            else:
                next_addr = node.right
            next_nodes.append(self.network[next_addr])
        return next_nodes

    def is_p2_done(self, current_nodes, num_steps):
        is_done = True
        for i, node in enumerate(current_nodes):  # type: NetworkNode
            addr = node.key  # type: str
            if addr.endswith('Z'):
                prev = self.cycle_tracker[i][-1]
                delta = num_steps - prev
                self.cycle_tracker[i].append(num_steps)
                print('{} @ Z: steps={}, delta={}'.format(i, num_steps, delta))
                if i == 0 and len(current_nodes) > 2:
                    if len(self.cycle_tracker[i]) == 3:
                        return 'cycle'
            else:
                is_done = False

            # check for cycle
        return is_done


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
