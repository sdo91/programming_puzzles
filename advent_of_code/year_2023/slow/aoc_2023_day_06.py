#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
Time:      7  15   30
Distance:  9  40  200
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    288,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    71503,
    None,
    None,
]

REAL_OUTPUTS = [
    440000,
    26187338
]


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        if part == 2:
            text = text.replace(' ', '')

        self.text = text
        lines = aoc_util.lines(self.text)
        self.times = aoc_util.ints(lines[0])
        self.records = aoc_util.ints(lines[1])

        self.num_races = len(self.times)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 1

        for i in range(self.num_races):
            num_ways = self.calc_num_ways(self.times[i], self.records[i])
            result *= num_ways

        return result

    def p2(self):
        return self.p1()

    def calc_num_ways(self, race_time, record):
        num_ways = 0
        for hold_time in range(1, race_time):
            if self.part == 2:
                AocLogger.log_percent(hold_time, race_time, 10)
            speed = hold_time
            move_time = race_time - hold_time
            distance = speed * move_time
            if distance > record:
                num_ways += 1
        return num_ways


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
