#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
import math

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    35,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    46,
    None,
    None,
]

REAL_OUTPUTS = [
    313045984,
    20283860
]


class GardenRange(object):
    part = 1

    def __init__(self, text: str):
        self.dest_start, self.src_start, self.length = aoc_util.ints(text)
        self.src_end = self.src_start + self.length - 1
        self.dest_end = self.dest_start + self.length - 1
        self.delta = self.dest_start - self.src_start

    def __repr__(self):
        if self.part == 1:
            return '{} -> {} (len={}, delta={})'.format(
                self.src_start,
                self.dest_start,
                self.length,
                self.delta
            )
        else:
            return 'dest={}, src={}, len={}'.format(
                self.dest_start,
                self.src_start,
                self.length
            )

    def __contains__(self, src: int):
        return self.src_start <= src <= self.src_end

    def __lt__(self, other):
        return self.dest_start < other.dest_start

    def resolve(self, key):
        return key + self.delta

    def contains_dest(self, dest):
        return self.dest_start <= dest <= self.dest_end

    def to_src(self, dest):
        return dest - self.delta


class GardenMap(object):
    def __init__(self, text: str):
        """
        seed-to-soil map:
        50 98 2
        52 50 48
        """
        lines = aoc_util.lines(text)
        self.name = lines[0].split()[0]

        self.ranges = []
        for line in lines:
            if 'map' in line:
                continue
            self.ranges.append(GardenRange(line))
        self.ranges = sorted(self.ranges)

    def __repr__(self):
        return '[GardenMap: {}]'.format(self.name)

    def resolve(self, key: int):
        result = key  # default case
        delta = 0
        for r in self.ranges:
            if key in r:
                result = r.resolve(key)
                delta = r.delta
                break
        AocLogger.log('{}: {} -> {} (delta={})'.format(
            self.name, key, result, delta
        ))
        return result

    def reverse(self, dest: int):
        """
        given a dest, calculate the src
        """
        src = dest  # default case
        for i, garden_range in enumerate(self.ranges):
            if garden_range.contains_dest(dest):
                src = garden_range.to_src(dest)
                break
        AocLogger.log('{}: {} (i={})'.format(
            self.name, src, i
        ))
        return src


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.blocks = self.text.split('\n\n')

        self.seeds = aoc_util.ints(self.blocks[0])
        self.is_example = len(self.seeds) == 4

        self.maps = []
        for block in self.blocks:
            if block.startswith('seeds:'):
                continue
            garden_map = GardenMap(block)
            self.maps.append(garden_map)
        self.reversed_maps = list(reversed(self.maps))

        AocLogger.log(str(self))

        self.unit_tests()

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def unit_tests(self):
        if self.part == 2:
            GardenRange.part = 2

            if self.is_example:
                location = 46
                seed = self.to_seed(location)
                assert seed == 82

                assert self.is_seed_in_p2(82)
            else:
                AocLogger.verbose = True
                location = REAL_OUTPUTS[1]
                seed = self.to_seed(location)
                assert seed == 3165202674

    def p1(self):
        GardenRange.part = 1
        result = math.inf

        for seed in self.seeds:
            location = self.to_location(seed)
            result = min(result, location)
            AocLogger.log('summary: {} -> {} (min={})\n'.format(
                seed, location, result
            ))

        return result

    def to_location(self, seed: int):
        AocLogger.log('seed: {}'.format(seed))
        current_index = seed
        for garden_map in self.maps:
            current_index = garden_map.resolve(current_index)
        return current_index

    def p2(self):
        """
        In the above example, the lowest location number can be obtained from
        seed number 82, which corresponds to
        soil 84,
        fertilizer 84,
        water 84,
        light 77,
        temperature 45,
        humidity 46,
        and location 46.

        So, the lowest location number is 46.
        """
        AocLogger.verbose = False
        GardenRange.part = 2

        if self.is_example:
            return self.find_best_location(0, 1)
        else:
            estimate = self.find_best_location(0, 100)
            new_start = estimate - 100
            hopeful_result = self.find_best_location(new_start, 1)
            return hopeful_result

    def find_best_location(self, start_location=0, resolution=1):
        result_location = start_location
        while True:
            AocLogger.log_percent(result_location, REAL_OUTPUTS[1])
            seed = self.to_seed(result_location)
            if self.is_seed_in_p2(seed):
                return result_location
            result_location += resolution

    def is_seed_in_p2(self, seed):
        """
        79 14 55 13
        """
        for i in range(0, len(self.seeds), 2):
            start = self.seeds[i]
            length = self.seeds[i + 1]
            end = start + length - 1
            if start <= seed <= end:
                return True
        return False

    def to_seed(self, location):
        AocLogger.log('\nlocation: {}'.format(location))
        current_index = location
        for garden_map in self.reversed_maps:
            current_index = garden_map.reverse(current_index)
        seed = current_index
        return seed


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
