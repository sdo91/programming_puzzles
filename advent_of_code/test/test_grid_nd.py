from unittest import TestCase
from util.grid_nd import GridND


class Test(TestCase):

    def test_basics(self):
        grid = GridND(3)

        grid.set((1, 2, 3), 'A')
        grid.set((0, 0, 0), 'B')
        grid.set((1, 2, -4), 'C')

        self.assertEqual('A', grid.get((1, 2, 3)))
        self.assertEqual('B', grid.get((0, 0, 0)))
        self.assertEqual('C', grid.get((1, 2, -4)))
        self.assertEqual(' ', grid.get((2, 3, 4)))

        self.assertEqual(4, 2 + 2)
        self.assertNotEqual(5, 2 + 2)

        z = 0


Test().test_basics()
