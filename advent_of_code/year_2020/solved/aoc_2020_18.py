#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
1 + 2 * 3 + 4 * 5 + 6    
    """, """
1 + (2 * 3) + (4 * (5 + 6))
    """, """
2 * 3 + (4 * 5)
    """, """
5 + (8 * 3 + 9 + 3 * 4 * 3)
    """, """
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
    """, """
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2
    """
]

TEST_OUTPUT_1 = [
    71,
    51,
    26,
    437,
    12240,
    13632,
]

TEST_OUTPUT_2 = [
    231,
    51,
    46,
    1445,
    669060,
    23340,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            14006719520523,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            545115449981968,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        part_1_result = 0
        for line in aoc_util.lines(text):
            solver = Solver(line)
            part_1_result += solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        part_2_result = 0
        for line in aoc_util.lines(text):
            solver = Solver(line)
            part_2_result += solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        # self.lines = aoc_util.lines(text)
        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        return self.evaluate(self.text, do_add_first=False)

    def evaluate(self, expression_str, do_add_first):
        """
        4 * (5 + 6)
        algo:
            while ):
                find the first )
                find he prev (
                eval and replace

            # now no more parens
            tokenize
            eval LTR
        """

        while ')' in expression_str:
            first_end = expression_str.find(')')
            matching_start = expression_str.rfind('(', 0, first_end)
            inside_parens = expression_str[matching_start + 1:first_end]

            evaluated = str(self.evaluate(inside_parens, do_add_first))

            with_parens = '({})'.format(inside_parens)
            expression_str = expression_str.replace(with_parens, evaluated, 1)

        if do_add_first:
            return self.eval_add_first(expression_str)

        tokens = expression_str.split()

        result = int(tokens[0])

        i = 1
        while i < len(tokens):
            operator = tokens[i]
            operand = tokens[i + 1]
            i += 2

            if operator == '+':
                result += int(operand)
            else:
                result *= int(operand)

        return result

    def p2(self):
        return self.evaluate(self.text, do_add_first=True)

    def eval_add_first(self, expression_str):
        """
        algo:
            split on *
            multiply
        """
        tokens = aoc_util.split_and_strip_each(expression_str, '*')
        result = 1
        for expr in tokens:
            result *= self.evaluate(expr, do_add_first=False)
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
