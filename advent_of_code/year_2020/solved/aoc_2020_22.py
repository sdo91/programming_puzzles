#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import deque

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    306,
    0,
    0,
]

TEST_OUTPUT_2 = [
    291,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            34005,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            32731,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class GameResult(object):
    def __init__(self):
        self.winner = -1
        self.winning_deck = deque()


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        # self.lines = aoc_util.lines(text)

        p1_text, p2_text = self.text.split('\n\n')
        self.p1_deck = self.to_deck(p1_text)
        self.p2_deck = self.to_deck(p2_text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def to_deck(self, text):
        ints = aoc_util.ints(text)
        deck = deque(ints[1:])
        return deck

    def p1(self):
        while True:
            p1_card = self.p1_deck.popleft()
            p2_card = self.p2_deck.popleft()

            winner = max(p1_card, p2_card)
            loser = min(p1_card, p2_card)

            if p1_card > p2_card:
                self.p1_deck.append(winner)
                self.p1_deck.append(loser)
            else:
                self.p2_deck.append(winner)
                self.p2_deck.append(loser)

            if not self.p1_deck:
                winning_deck = self.p2_deck
                break
            elif not self.p2_deck:
                winning_deck = self.p1_deck
                break

        # calc score
        score = self.calc_score(winning_deck)
        return score

    @classmethod
    def calc_score(cls, deck):
        score = 0
        for x in range(len(deck), 0, -1):
            card_score = (x * deck[-x])
            score += card_score
        return score

    def p2(self):
        result = self.recursive_combat(self.p1_deck.copy(), self.p2_deck.copy())
        score = self.calc_score(result.winning_deck)
        return score

    @classmethod
    def recursive_combat(cls, deck1: deque, deck2: deque) -> GameResult:
        prev_states = set()
        game_result = GameResult()

        while True:
            # check state
            state = '{}/{}'.format(deck1, deck2)
            if state in prev_states:
                # print('stalemate!')
                game_result.winner = 1
                # todo: winning deck?
                return game_result
            prev_states.add(state)

            # draw cards
            card1 = deck1.popleft()
            card2 = deck2.popleft()

            # check if we can play a sub game
            if len(deck1) >= card1 and len(deck2) >= card2:
                # play subgame
                subdeck1 = cls.copy_from_deck(deck1, card1)
                subdeck2 = cls.copy_from_deck(deck2, card2)
                sub_result = cls.recursive_combat(subdeck1, subdeck2)
                round_winner = sub_result.winner
            else:
                # winner is the high card
                if card1 > card2:
                    round_winner = 1
                else:
                    round_winner = 2

            # place the cards
            if round_winner == 1:
                winning_card = card1
                losing_card = card2
                deck1.append(winning_card)
                deck1.append(losing_card)
            else:
                winning_card = card2
                losing_card = card1
                deck2.append(winning_card)
                deck2.append(losing_card)

            # check if game is over
            if not deck1:
                game_result.winner = 2
                game_result.winning_deck = deck2
                return game_result
            elif not deck2:
                game_result.winner = 1
                game_result.winning_deck = deck1
                return game_result

            z = 0

    @classmethod
    def copy_from_deck(cls, deck: deque, num_cards: int) -> deque:
        as_list = list(deck)
        my_slice = as_list[:num_cards]
        return deque(my_slice)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
