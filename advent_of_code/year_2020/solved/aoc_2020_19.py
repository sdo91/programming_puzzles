#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import re
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
0: 1 2
1: "a"
2: 1 3 | 3 1
3: "b"

aaa
aab
aba
abb
baa
bab
bba
bbb
    """, """
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb
    """, """
42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
    """
]

TEST_OUTPUT_1 = [
    2,
    2,
    3,
]

TEST_OUTPUT_2 = [
    None,
    None,
    12,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            299,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            414,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        # self.lines = aoc_util.lines(text)

        self.rules_text, self.msgs_text = aoc_util.split_and_strip_each(self.text, '\n\n')

        self.rules_lines = aoc_util.split_and_strip_each(self.rules_text, '\n')

        self.rules_dict = {}
        for line in self.rules_lines:
            key, value = line.split(': ')
            self.rules_dict[key] = value

        self.known_rules = {}

        self.messages = aoc_util.split_and_strip_each(self.msgs_text, '\n')

        # for p2
        if '42' in self.rules_dict:
            self.len_r42 = self.count_chars(self.rules_dict['42'])
            self.len_r31 = self.count_chars(self.rules_dict['31'])

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        rule_text = self.rules_dict['0']
        regex_pattern = self.build_regex(rule_text)

        result = 0
        for msg in self.messages:
            if re.fullmatch(regex_pattern, msg):
                result += 1

        return result

    def p2(self):
        """
        special rules:
        8: 42 | 42 8
            42+
        11: 42 31 | 42 11 31
            42 42 42 31 31 31

        note: rules 8/11 only used in rule 0
        0: 8 11
        so:
        0: 42+ 42n 31n (where n is 1 +)

        42 has to appear more times than 31
        """
        # did this part by hand
        rule_text = '42+ 31+'

        regex_pattern = self.build_regex(rule_text)

        result = 0
        for msg in self.messages:
            if re.fullmatch(regex_pattern, msg):
                if self.check_42_gt_31(msg):
                    result += 1

        return result

    def build_regex(self, rule_text: str) -> str:
        """
        algo:
            3 types:
                direct
                or
                append

        0: 1 2
        1: "a"
        2: 1 3 | 3 1
        3: "b"
        """
        # todo: memoize?

        if '"' in rule_text:
            # direct
            result = rule_text.replace('"', '')
        elif '|' in rule_text:
            # or
            half1, half2 = rule_text.split(' | ')
            result = '({}|{})'.format(
                self.build_regex(half1),
                self.build_regex(half2)
            )
        else:
            # append
            keys = rule_text.split()
            result_parts = []
            for k in keys:
                do_plus = '+' in k
                if do_plus:
                    k = k.replace('+', '')
                text = self.rules_dict[k]
                part = self.build_regex(text)
                if do_plus:
                    part += '+'
                result_parts.append(part)
            result = ''.join(result_parts)

        self.known_rules[rule_text] = result
        return result

    def check_42_gt_31(self, msg: str) -> bool:
        """
        looks like len_r42 == len_r31
        this means we can just divide the msg into chunks, and the middle chunk(s) should match r42
        """
        # this must be true for this method to work
        assert self.len_r31 == self.len_r42

        # make sure we have an odd number of chunks
        chunk_size = self.len_r42
        num_chunks = len(msg) // chunk_size
        if num_chunks % 2 == 0:
            # remove first chunk to make odd
            msg = msg[chunk_size:]

        # get the middle chunk
        end_size = (len(msg) - chunk_size) // 2
        middle = msg[end_size:-end_size]

        # check if it matches rule 42
        rule_42 = self.known_rules[self.rules_dict['42']]
        is_match = re.fullmatch(rule_42, middle) is not None
        return is_match

    def count_chars(self, rule_text: str) -> int:
        """
        '((b(a(bb|ab)|b(a|b)(a|b))|a(bbb|a(bb|a(a|b))))b|(((aa|ab)a|bbb)b|((a|b)a|bb)aa)a)'

        0: 1 2
        1: "a"
        2: 1 3 | 3 1
        3: "b"
        """
        if '"' in rule_text:
            # direct
            return 1
        elif '|' in rule_text:
            # or
            half1, half2 = rule_text.split(' | ')
            count1 = self.count_chars(half1)
            count2 = self.count_chars(half2)
            assert count1 == count2  # this assumption must be true
            return count1
        else:
            # tokens
            keys = rule_text.split()
            my_sum = 0
            for k in keys:
                text = self.rules_dict[k]
                my_sum += self.count_chars(text)
            return my_sum
    # append


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
