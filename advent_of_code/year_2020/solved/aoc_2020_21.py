#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    5,
    0,
    0,
]

TEST_OUTPUT_2 = [
    'mxmxvkd,sqjhc,fvjkl',
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            1685,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            'ntft,nhx,kfxr,xmhsbd,rrjb,xzhxj,chbtp,cqvc',
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Food(object):

    def __init__(self, text):
        text = text.replace('(', '')
        text = text.replace(')', '')
        text = text.replace(',', '')
        ingredients, allergens = aoc_util.split_and_strip_each(text, 'contains')

        self.ingredients = set(ingredients.split())
        self.allergens = set(allergens.split())

    def __repr__(self):
        return '[Food: {} {}]'.format(self.ingredients, self.allergens)


class Solver(object):

    def __init__(self, text: str):
        """
        mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
        trh fvjkl sbzzf mxmxvkd (contains dairy)
        sqjhc fvjkl (contains soy)
        sqjhc mxmxvkd sbzzf (contains fish)
        """
        self.text = text.strip()
        self.lines = aoc_util.lines(text)

        self.foods = []
        self.allergens = set()
        self.ingredients = set()

        for line in self.lines:
            f = Food(line)
            self.foods.append(f)
            self.allergens |= f.allergens
            self.ingredients |= f.ingredients

        self.known_matches_by_allergen = {}
        self.matched_ingredients = set()
        self.do_shared()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def do_shared(self):
        """
        algo:
            for each allergen (dairy)
                find each line with it
                find ingredients common to those lines
                those become potentials
            loop thru removing knowns until no unknowns
        """
        potentials_by_allergen = {}

        for allergen in self.allergens:
            potentials_by_allergen[allergen] = self.find_potentials(allergen)

        # reduce
        while len(self.known_matches_by_allergen) < len(self.allergens):
            for allergen in self.allergens:
                if allergen in self.known_matches_by_allergen:
                    continue  # already found

                # if we get here,
                # the match not yet known for this allergen

                # remove any ingredients that have already been matched
                # new_set = old_set - matched
                potentials = potentials_by_allergen[allergen]
                potentials -= self.matched_ingredients

                # now check if there is just one left
                if len(potentials) == 1:
                    ingredient = next(iter(potentials))
                    print('match found: {} & {}'.format(allergen, ingredient))
                    self.known_matches_by_allergen[allergen] = ingredient
                    self.matched_ingredients.add(ingredient)

    def p1(self):
        num_safe = 0
        for f in self.foods:
            safe = f.ingredients - self.matched_ingredients
            num_safe += len(safe)
        return num_safe

    def p2(self):
        alpha = sorted(self.known_matches_by_allergen.keys())
        canonical_dangerous_ingredient_list = [self.known_matches_by_allergen[a] for a in alpha]
        formatted = ','.join(canonical_dangerous_ingredient_list)
        print('p2: ' + formatted)
        return formatted

    def find_potentials(self, allergen):
        potentials = self.ingredients.copy()
        for food in self.foods:
            if allergen in food.allergens:
                # check ingredients
                potentials &= food.ingredients
        return potentials


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
