#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

from typing import List
import aocd
import time
import traceback

try:
    from llist import dllist, dllistnode
except ImportError:
    from pyllist import dllist, dllistnode

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
389125467
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    67384529,
    0,
    0,
]

TEST_OUTPUT_2 = [
    149245887792,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        # aoc_util.assert_equal(
        #     25398647,
        #     self.solve_part_1(self.puzzle_input)
        # )

        # aoc_util.assert_equal(
        #     0,
        #     self.solve_part_2(self.puzzle_input)
        # )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        self.debug_p2(TEST_INPUT[0])
        # aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result

    def debug_p2(self, text: str):
        solver = Solver(text)
        solver.debug_p2()


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.digits = aoc_util.digits(self.text)

        # p1
        self.cups_p1 = self.digits.copy()
        self.current_cup_p1 = self.cups_p1[0]

        # p2
        self.cups_linked = dllist()
        self.nodes_by_id = {}
        self.current_cup_p2 = self.current_cup_p1

        # alt method 3
        self.cups3 = CrabCups3(self.digits.copy())

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        self.play_game_slow(num_iterations=100)
        result = self.format_result(self.cups_p1)
        return result

    def play_game_slow(self, num_iterations):
        for i in range(1, num_iterations + 1):
            self.do_move_slow(i, verbose=True)

        return self.cups_p1

    def do_move_slow(self, move_number, verbose):
        current_idx = self.cups_p1.index(self.current_cup_p1)

        if verbose:
            print('-- move {} (slow) --\ncups: {}'.format(
                move_number,
                self.format_cups(self.cups_p1, self.current_cup_p1)
            ))
        else:
            print('move_number={}'.format(move_number))

        # pick up 3 cups
        self.cups_p1, picked = self.pick_up(self.cups_p1, current_idx)
        if verbose:
            print('pick up: {}'.format(picked))

        # select destination cup
        destination_cup = self.decrement(self.current_cup_p1, picked)
        if verbose:
            print('destination: {}'.format(destination_cup))

        # place the 3 cups
        self.cups_p1 = self.insert(self.cups_p1, picked, destination_cup)

        # rotate (NOTE: only needed to line up with example)
        if verbose:
            self.cups_p1 = self.rotate(self.cups_p1, self.current_cup_p1, current_idx)

        # select new current cup
        current_idx = self.cups_p1.index(self.current_cup_p1)
        current_idx += 1
        if current_idx == len(self.cups_p1):
            current_idx = 0  # wrap
        self.current_cup_p1 = self.cups_p1[current_idx]

        if verbose:
            print()

    def pick_up(self, cups: List[int], current_idx: int) -> (List[int], List[int]):
        diff = len(cups) - current_idx
        if diff <= 3:
            # rotate to avoid edge case
            cups = aoc_util.rotate_left(cups, current_idx)
            current_idx = 0

        picked_start = current_idx + 1
        picked_end = current_idx + 4

        picked = cups[picked_start:picked_end]

        assert len(picked) == 3

        before = cups[:picked_start]
        after = cups[picked_end:]

        return before + after, picked

    def decrement(self, current_cup, picked):
        result = current_cup
        while True:
            result -= 1
            if result == 0:
                result = 9  # wrap
            if result not in picked:
                return result

    def insert(self, cups, picked, destination_cup):
        """
        insert picked into cups after destination cup
        """
        destination_idx = cups.index(destination_cup)
        insert_index = destination_idx + 1
        # don't think I need to wrap
        before = cups[:insert_index]
        after = cups[insert_index:]
        return before + picked + after

    def format_cups(self, cups, current_cup):
        cups_str = str(cups)
        cups_str = cups_str.replace(',', '')
        if len(cups) < 10:
            old_str = str(current_cup)
            new_str = '({})'.format(old_str)
            cups_str = cups_str.replace(old_str, new_str)
        return cups_str

    def rotate(self, cups, current_cup, desired_idx):
        if cups[desired_idx] == current_cup:
            return cups
        else:
            actual_index = cups.index(current_cup)
            left_shift = actual_index - desired_idx
            cups = aoc_util.rotate_left(cups, left_shift)
            return cups

    def format_result(self, cups):
        idx = cups.index(1)

        cups = aoc_util.rotate_left(cups, idx)
        as_str = str(cups[1:])

        as_str = aoc_util.remove_chars(as_str, '[, ]')
        return int(as_str)

    def init_cups_p1(self, num_cups):
        next_cup = 10
        while len(self.cups_p1) < num_cups:
            self.cups_p1.append(next_cup)
            next_cup += 1
        assert len(self.cups_p1) == num_cups

    def get_p2_result_slow(self):
        idx_1 = self.cups_p1.index(1)
        idx_star1 = (idx_1 + 1) % len(self.cups_p1)
        idx_star2 = (idx_1 + 2) % len(self.cups_p1)
        return self.cups_p1[idx_star1] * self.cups_p1[idx_star2]

    def p2(self):
        """

        """
        # num_cups = 9
        num_cups = 1000000

        # num_iterations = 10
        num_iterations = 10000000

        self.init_cups_p2(num_cups)

        self.play_game_fast(num_iterations)

        return self.get_p2_result_fast()

    def init_cups_p2(self, num_cups):
        for cup_id in self.digits:
            self.insert_end(cup_id)
        if num_cups > len(self.cups_linked):
            for cup_id in range(10, num_cups + 1):
                self.insert_end(cup_id)
            z = 0
        assert len(self.cups_linked) == num_cups

    def get_p2_result_fast(self):
        # get star nodes
        node1 = self.nodes_by_id[1]  # type: dllistnode
        star1_node = self.get_next_node_wrapped(node1)
        star2_node = self.get_next_node_wrapped(star1_node)

        print('end: {}'.format([node1, star1_node, star2_node]))
        return star1_node.value * star2_node.value

    def get_next_node_wrapped(self, node: dllistnode) -> dllistnode:
        if node.next is None:
            return self.cups_linked.first
        else:
            return node.next

    def insert_end(self, cup_id):
        self.cups_linked.append(cup_id)
        self.nodes_by_id[cup_id] = self.cups_linked.last

    def insert_after(self, picked, destination_cup):
        dest_node = self.nodes_by_id[destination_cup]
        for cup in picked:
            new_node = self.cups_linked.insertafter(cup, dest_node)
            self.nodes_by_id[cup] = new_node
            dest_node = new_node

    def play_game_fast(self, num_iterations):
        verbose = num_iterations < 1000

        for i in range(1, num_iterations + 1):
            self.do_move_fast(i, verbose)

        if verbose:
            print(self.cups_linked)

    def do_move_fast(self, move_number, verbose):
        if verbose:
            print('-- move {} (fast) --\ncups: {}'.format(move_number, self.cups_linked))
        elif move_number % 100000 == 0:
            print('{}%'.format(move_number // 100000))

        # pick up 3 cups
        picked = self.fast_pick_up(self.current_cup_p2)
        if verbose:
            print('pick up: {}'.format(picked))

        # select destination cup
        destination_cup = self.decrement(self.current_cup_p2, picked)
        if verbose:
            print('destination: {}'.format(destination_cup))

        # place the 3 cups
        self.insert_after(picked, destination_cup)

        # select new current cup
        self.current_cup_p2 = self.fast_increment_current(self.current_cup_p2)

        if verbose:
            print()

    def fast_pick_up(self, current_cup):
        picked = []
        current_node = self.nodes_by_id[current_cup]  # type: dllistnode
        for x in range(3):
            # check for end edge case
            if self.cups_linked.last() == current_cup:
                # current is the last node: grab from front
                next_node = self.cups_linked.first
            else:
                # normal: grab next node
                next_node = current_node.next
            cup = self.cups_linked.remove(next_node)
            self.nodes_by_id[cup] = None  # just in case
            picked.append(cup)
        return picked

    def fast_increment_current(self, current_cup):
        """
        if last:
            choose first
        else:
            choose next
        """
        if self.cups_linked.last() == current_cup:
            return self.cups_linked.first()
        else:
            current_node = self.nodes_by_id[current_cup]
            next_cup = current_node.next()
            return next_cup

    def debug_p2(self):
        """
        algo:
        do a move slow
        do a move fast
        check that they are the same

        then scale
        """
        num_cups = 9
        num_iterations = 100
        verbose = True

        # num_cups = 1000000
        # num_iterations = 1000
        # verbose = False

        self.init_cups_p1(num_cups)
        self.init_cups_p2(num_cups)
        self.cups3.init_cups(num_cups)

        for i in range(num_iterations):
            move_number = i + 1
            self.do_move_slow(move_number, verbose)
            self.do_move_fast(move_number, verbose)
            self.cups3.do_move(move_number, verbose)
            self.compare_fast_and_slow(move_number)
            print('\n\n')

    def compare_fast_and_slow(self, move_number):
        """
        todo: check method 3
        """
        try:
            assert self.current_cup_p1 == self.current_cup_p2
            assert len(self.cups_p1) == len(self.cups_linked)

            first_cup_p2 = self.cups_linked.first()

            index_p1 = self.cups_p1.index(first_cup_p2)

            for p2_cup in self.cups_linked:
                p1_cup = self.cups_p1[index_p1]

                assert p1_cup == p2_cup

                index_p1 += 1
                index_p1 %= len(self.cups_p1)

            p2_result_slow = self.get_p2_result_slow()
            p2_result_fast = self.get_p2_result_fast()
            assert p2_result_slow == p2_result_fast

        except AssertionError:
            print('issue found on move: {}'.format(move_number))
            z = 0


class CrabCups3(object):

    def __init__(self, start_cups):
        self.start_cups = start_cups

        self.cups = []
        self.current_cup = self.start_cups[0]

    def __repr__(self):
        temp = [1]
        while True:
            prev_cup = temp[-1]
            next_cup = self.next_cup(prev_cup)
            if next_cup == 1:
                break
            temp.append(next_cup)
        return '{} (current={})'.format(temp, self.current_cup)

    def init_cups(self, num_cups):
        self.cups = [-1] * num_cups
        for idx in range(num_cups):
            next_idx = (idx + 1) % num_cups
            cup = self.start_cups[idx] % num_cups
            next_cup = self.start_cups[next_idx] % num_cups
            self.cups[cup] = next_cup

    def next_cup(self, cup=None):
        if cup is None:
            cup = self.current_cup
        return self.cups[cup]

    def do_move(self, move_number, verbose):
        if verbose:
            print('-- move {} (3) --\ncups: {}'.format(
                move_number,
                str(self)
            ))

        # pick up 3 cups
        picked = [self.next_cup()]
        while len(picked) < 3:
            next_picked = self.next_cup(picked[-1])
            picked.append(next_picked)
        self.cups[self.current_cup] = self.cups[picked[-1]]
        if verbose:
            print('pick up: {}'.format(picked))

        # select destination cup
        destination_cup = self.select_destination(picked)
        if verbose:
            print('destination: {}'.format(destination_cup))

        # place the 3 cups
        temp = self.next_cup(destination_cup)
        self.cups[destination_cup] = picked[0]
        self.cups[picked[0]] = picked[1]
        self.cups[picked[1]] = picked[2]
        self.cups[picked[2]] = temp

        # select new current cup
        self.current_cup = self.next_cup()

    def select_destination(self, picked):
        result = self.current_cup
        while True:
            result -= 1
            result %= len(self.cups)
            if result not in picked:
                return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
