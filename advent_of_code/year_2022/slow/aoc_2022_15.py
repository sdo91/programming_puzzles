#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
Sensor at x=8, y=7: closest beacon is at x=2, y=10
    """, """
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3
    """, """

    """
]

TEST_OUTPUT_1 = [
    12,
    26,
    None,
]

TEST_OUTPUT_2 = [
    None,
    56000011,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False
        Solver.is_test = False

        aoc_util.assert_equal(
            5367037,
            self.solve_part_1(self.puzzle_input, 2000000)
        )

        aoc_util.assert_equal(
            11914583249288,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str, row=10):
        solver = Solver(text)

        part_1_result = solver.p1(row)

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):
    is_test = True

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        self.grid = Grid2D(default='.')

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self, row):
        """
        example:
        Sensor at x=8, y=7: closest beacon is at x=2, y=10
        """
        i = 0
        for line in self.lines:
            AocLogger.log_percent(i, self.lines, 1)
            i += 1

            sx, sy, bx, by = aoc_util.ints(line)
            sensor_coord = (sx, sy)
            beacon_coord = (bx, by)
            dist = aoc_util.manhatten_dist(sensor_coord, beacon_coord)
            print('md={}'.format(dist))

            # self.grid.set_all_within_manhatten_dist(sensor_coord, dist, '#')  # too slow
            self.mark_row(row, sensor_coord, dist)

        # mark beacons last
        for line in self.lines:
            sx, sy, bx, by = aoc_util.ints(line)
            beacon_coord = (bx, by)
            self.grid.set_tuple(beacon_coord, 'B')

        if AocLogger.verbose:
            self.grid.show_from((-4, 9), (26, 11))
        result = self.grid.count_in_row('#', row)
        return result

    def mark_row(self, y, sensor_coord, dist):
        vertical_dist = abs(sensor_coord[1] - y)
        horiz_dist = dist - vertical_dist
        x1 = sensor_coord[0] - horiz_dist
        x2 = sensor_coord[0] + horiz_dist
        for x in range(x1, x2 + 1):
            coord = (x, y)
            self.grid.set_tuple(coord, '#')

    def p2(self):
        # parse input
        checked = set()
        sensors = []
        for line in self.lines:
            sensor = Sensor(line)
            sensors.append(sensor)

            checked.add(sensor.beacon_coord)  # we don't need to check beacons

        # for each sensor
        for i in aoc_util.range_len(sensors):
            sensor = sensors[i]
            print()
            print('checking {}/{}: {}'.format(i + 1, len(sensors), sensor.line))

            # check all edge coords
            edge_coords = sensor.calc_edge_coords()
            print('num edge coords: {}'.format(len(edge_coords)))
            j = 0
            for edge_coord in edge_coords:
                AocLogger.log_percent(j, edge_coords, 10)
                j += 1
                for coord in Grid2D.get_adjacent_coords(edge_coord):
                    if coord in checked:
                        continue

                    # check bounds
                    max_bound = 20 if self.is_test else 4000000
                    if coord[0] <= 0 or coord[0] >= max_bound:
                        continue
                    if coord[1] <= 0 or coord[1] >= max_bound:
                        continue

                    # check the coord
                    if self.is_distress_signal(coord, sensors):
                        return 4000000 * coord[0] + coord[1]
                    checked.add(coord)

        assert False

    @classmethod
    def is_distress_signal(cls, coord, sensors):
        """
        algo:
        for each:
            check if in diamond
        if not in any: found
        """
        for sensor in sensors:
            m_dist = aoc_util.manhatten_dist(sensor.sensor_coord, coord)
            if m_dist <= sensor.manhatten_dist:
                return False
        return True


class Sensor(object):

    def __init__(self, line):
        self.line = line
        sx, sy, bx, by = aoc_util.ints(line)
        self.sensor_coord = (sx, sy)
        self.beacon_coord = (bx, by)
        self.manhatten_dist = aoc_util.manhatten_dist(self.sensor_coord, self.beacon_coord)

    def calc_edge_coords(self):
        print('manhatten_dist={}'.format(self.manhatten_dist))
        return Grid2D.calc_all_at_manhatten_dist(self.sensor_coord, self.manhatten_dist)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
