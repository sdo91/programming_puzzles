#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
    """, """
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
    """, """

    """
]

TEST_OUTPUT_1 = [
    13,
    None,
    None,
]

TEST_OUTPUT_2 = [
    1,
    36,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            5710,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            2259,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        head = (0, 0)
        tail = (0, 0)
        positions = {tail}  # keep track of tail positions
        for line in self.lines:
            # parse line
            direction, amount = line.split()
            delta = Grid2D.RELATIVE_DIRECTIONS[direction]
            amount = int(amount)

            # do the moves
            for _ in range(amount):
                # move head
                head = Grid2D.adjust_coord(head, *delta)

                # move tail
                tail = self.move_knot(head, tail)
                positions.add(tail)

        return len(positions)

    def move_knot(self, head, tail):
        """
        x123x
        11233
        44T55
        66788
        x678x
        """
        if Grid2D.are_adjacent(head, tail, include_diagonal=True):
            # don't need to move
            return tail

        vector_to_head = aoc_util.tuple_subtract(head, tail)  # ex: 1, -2
        dx = aoc_util.sign(vector_to_head[0])
        dy = aoc_util.sign(vector_to_head[1])
        return Grid2D.adjust_coord(tail, dx, dy)

    def p2(self):
        rope = [(0, 0)] * 10
        positions = {rope[-1]}  # keep track of tail positions

        for line in self.lines:
            # parse line
            direction, amount = line.split()
            head_delta = Grid2D.RELATIVE_DIRECTIONS[direction]
            amount = int(amount)

            # do the moves
            for _ in range(amount):
                self.simulate_rope_p2(rope, head_delta)
                positions.add(rope[-1])

        return len(positions)

    def simulate_rope_p2(self, rope, head_delta):
        # move head
        rope[0] = Grid2D.adjust_coord(rope[0], *head_delta)

        for i in range(1, len(rope)):
            # move knot
            rope[i] = self.move_knot(rope[i - 1], rope[i])


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
