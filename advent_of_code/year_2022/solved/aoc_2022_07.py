#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import os
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    95437,
    None,
    None,
]

TEST_OUTPUT_2 = [
    24933642,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            1306611,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            13210366,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class AocFile(object):
    def __init__(self, name, size, path):
        self.name = name
        self.size = int(size)
        self.path = path

    def __repr__(self):
        return '{{AocFile: {}, {}}}'.format(self.name, self.size)


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

        self.file_structure = {}
        self.current_dir = '/'
        self.dir_set = set()

        self.parse_files()

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_files(self):
        """
        cd x moves in one level
        cd .. moves out one level
        cd / switches the current directory to the outermost directory, /
        ls means list
        123 abc means that the current directory contains a file named abc with size 123.
        dir xyz means that the current directory contains a directory named xyz.
        """
        # parse file structure
        for line in self.lines:
            if line.startswith('$ cd'):
                arg = line.split()[2]
                if arg == '..':
                    self.current_dir = self.get_parent(self.current_dir)
                elif arg == '/':
                    self.current_dir = '/'
                else:
                    self.current_dir = os.path.join(self.current_dir, arg)
                self.dir_set.add(self.current_dir)
                AocLogger.log('{}: {}'.format(line, self.current_dir))
            elif line.startswith('$ ls'):
                pass
            else:
                # a file or dir
                size, name = line.split()
                if size == 'dir':
                    pass
                else:
                    # add to dict
                    self.add_file(name, size, self.current_dir)

    def add_file(self, name, size, path):
        # get folder
        folder = self.get_dir(path)

        # add new file
        folder[name] = AocFile(name, size, path)

    def get_dir(self, dir_path):
        if dir_path == '/':
            dirs_from_home = []
        else:
            dirs_from_home = dir_path[1:].split('/')
        folder = self.file_structure
        for dir_basename in dirs_from_home:
            if dir_basename not in folder:
                folder[dir_basename] = {}
            folder = folder[dir_basename]
        return folder

    @classmethod
    def get_parent(cls, child_path):
        return os.path.abspath(os.path.join(child_path, os.pardir))

    def calc_dir_size(self, folder):
        result_size = 0
        for entry in folder.values():
            if type(entry) == AocFile:
                result_size += entry.size
            else:  # dict
                result_size += self.calc_dir_size(entry)
        return result_size

    def p1(self):
        result = 0
        for dir_path in self.dir_set:
            folder = self.get_dir(dir_path)
            dir_size = self.calc_dir_size(folder)
            if dir_size <= 100000:
                result += dir_size
        return result

    def p2(self):
        """
        The total disk space available to the filesystem is 70000000.
        To run the update, you need unused space of at least 30000000.
        You need to find a directory you can delete that will free up enough space to run the update.

        Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update.
        What is the total size of that directory?
        """
        MAX_USEABLE_SPACE = 70000000 - 30000000

        total_used = self.calc_dir_size(self.get_dir('/'))
        amount_needed = total_used - MAX_USEABLE_SPACE

        smallest = 9e9
        for dir_path in self.dir_set:
            folder = self.get_dir(dir_path)
            dir_size = self.calc_dir_size(folder)

            if dir_size >= amount_needed:
                smallest = min(smallest, dir_size)

        return smallest


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
