#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import string
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.dijkstra_solver import DijkstraSolver, Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    31,
    None,
    None,
]

TEST_OUTPUT_2 = [
    29,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            449,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            443,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.grid = Grid2D(self.text)
        if AocLogger.verbose:
            self.grid.show()

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        pathfinder = HeightmapPathfinder(self.grid)

        start_coord = self.grid.find_only('S')
        node = pathfinder.find_shortest_path(start_coord)

        result = len(node.path)
        return result

    def p2(self):
        """
        assumption: there will be a path shorter than p1, so I don't need to check 'S'

        originally I checked all paths starting at 'a', but it's much faster to go backwards
        """
        pathfinder = HeightmapPathfinder(self.grid, is_p1=False)

        start_coord = self.grid.find_only('E')
        node = pathfinder.find_shortest_path(start_coord)

        result = len(node.path)
        return result


class HeightmapPathfinder(DijkstraSolver):

    def __init__(self, grid: Grid2D, is_p1=True):
        self.is_p1 = is_p1
        open_chars = set(string.ascii_lowercase)
        if is_p1:
            goal_chars = {'E'}
        else:
            goal_chars = {'a'}
        super().__init__(grid, open_chars, goal_chars)

    def get_adjacent(self, coord):
        result = []
        for adjacent in self.grid.get_adjacent_coords(coord):
            if self.grid.is_out_of_bounds(adjacent):
                continue
            height_diff = self.get_height(adjacent) - self.get_height(coord)
            if self.is_height_difference_ok(height_diff):
                result.append(adjacent)
        return result

    def is_height_difference_ok(self, height_diff):
        if self.is_p1:
            return height_diff <= 1
        else:
            return height_diff >= -1

    def get_height(self, coord):
        letter = self.grid.get_tuple(coord)
        if letter == 'S':
            letter = 'a'
        elif letter == 'E':
            letter = 'z'
        return ord(letter) - ord('a')


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
