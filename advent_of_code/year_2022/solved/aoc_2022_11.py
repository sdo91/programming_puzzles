#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    10605,
    None,
    None,
]

TEST_OUTPUT_2 = [
    2713310158,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            50172,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            11614682178,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.chunks = self.text.split('\n\n')
        self.monkeys = {}
        for chunk in self.chunks:
            monkey = Monkey(chunk)
            self.monkeys[monkey.id] = monkey

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        Monkey.is_p1 = True
        monkey_business = self.calc_monkey_business(20)
        return monkey_business

    def calc_monkey_business(self, num_rounds, show_after=frozenset()):
        for i in range(1, num_rounds + 1):
            self.do_round()

            if i in show_after:
                AocLogger.log('== After round {} =='.format(i))
                for m in range(len(self.monkeys)):
                    self.monkeys[m].log_inspections()
                AocLogger.log()

            if not Monkey.is_p1 and not AocLogger.verbose:
                AocLogger.log_percent(i, num_rounds, 10)

        inspections = []
        for m in self.monkeys.values():
            inspections.append(m.num_inspections)
        inspections.sort()

        monkey_business = inspections[-2] * inspections[-1]
        return monkey_business

    def do_round(self):
        for i in range(len(self.monkeys)):
            active_monkey = self.monkeys[i]
            active_monkey.do_turn(self.monkeys)

    def p2(self):
        Monkey.is_p1 = False
        Monkey.p2_common_denom = self.calc_common_denom()

        num_rounds = 10000
        show_after = {1, 20} | set(range(1000, 10001, 1000))
        monkey_business = self.calc_monkey_business(num_rounds, show_after)

        return monkey_business

    def calc_common_denom(self):
        result = 1
        for monkey in self.monkeys.values():
            result *= monkey.mod_number
        return result


class Monkey(object):
    is_p1 = True
    p2_common_denom = 1

    def __init__(self, text):
        self.text = aoc_util.lines(text)
        lines = aoc_util.lines(text)

        self.id = aoc_util.parse_only_int(lines[0])
        self.items = aoc_util.ints(lines[1])

        self.operation = lines[2].replace('  Operation: new = ', '')
        self.mod_number = aoc_util.parse_only_int(lines[3])

        self.recipients = {
            True: aoc_util.parse_only_int(lines[4]),
            False: aoc_util.parse_only_int(lines[5])
        }

        self.num_inspections = 0

    def __repr__(self):
        return 'Monkey {}: {}'.format(self.id, self.items)

    def do_turn(self, monkeys_dict):
        """
        Monkey inspects an item with a worry level of 79.
        Worry level is multiplied by 19 to 1501.
        Monkey gets bored with item. Worry level is divided by 3 to 500.
        Current worry level is not divisible by 23.
        Item with worry level 500 is thrown to monkey 3.
        """
        for item in self.items:
            self.num_inspections += 1

            worry_level = self.adjust_worry_level(item)

            if self.is_p1:
                worry_level //= 3  # gets bored
            else:
                # you'll need to find another way to keep your worry levels manageable
                worry_level %= self.p2_common_denom

            test_result = (worry_level % self.mod_number == 0)

            new_monkey_id = self.recipients[test_result]
            monkeys_dict[new_monkey_id].items.append(worry_level)

        self.items = []

    def adjust_worry_level(self, item):
        text = self.operation.replace('old', str(item))
        return eval(text)

    def log_inspections(self):
        AocLogger.log('Monkey {} inspected items {} times.'.format(self.id, self.num_inspections))


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
