#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    157,
    0,
    0,
]

TEST_OUTPUT_2 = [
    70,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            8072,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            2567,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def calc_pri(self, letter):
        # test = ('z' > 'A')
        # big_a = ord('A')
        # big_z = ord('Z')
        # little_a = ord('a')
        # little_z = ord('z')

        as_int = ord(letter)

        if as_int >= ord('a'):
            # lower
            result = as_int - ord('a') + 1
        else:
            # upper
            result = as_int - ord('A') + 27

        return result

    def p1(self):
        total = 0
        for line in self.lines:
            # divide into compartments
            half_len = int(len(line) / 2)
            c1 = set(line[:half_len])
            c2 = set(line[half_len:])

            # find the common letter
            common = c1 & c2
            letter = common.pop()

            # score it
            total += self.calc_pri(letter)

        return total

    def p2(self):
        total = 0
        for i in range(0, len(self.lines), 3):
            # parse the group
            e1 = set(self.lines[i])
            e2 = set(self.lines[i + 1])
            e3 = set(self.lines[i + 2])

            # find the common letter
            common = e1 & e2 & e3

            # score it
            total += self.calc_pri(common.pop())

        return total


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
