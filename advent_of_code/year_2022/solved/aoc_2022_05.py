#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
x   [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    'CMZ',
    0,
    0,
]

TEST_OUTPUT_2 = [
    'MCD',
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            'VRWBSFZWM',
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            'RBTWJWMCF',
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        if text.startswith('x'):
            # test input
            self.text = text.strip()
        else:
            self.text = text

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

        # find line w/ numbers
        i = 0
        for line in self.lines:
            if '[' not in line:
                self.number_line = i
                self.stack_numbers = aoc_util.ints(line)
                self.num_stacks = len(self.stack_numbers)
                break
            i += 1

        # init stacks
        self.stacks = []
        for i in range(self.num_stacks):
            self.stacks.append([])

        # read into stacks
        for i in range(self.number_line - 1, -1, -1):
            line = self.lines[i]
            self.parse_line(line)

        # read instructions
        self.instructions = []
        self.instructions_raw = []
        for i in range(self.number_line + 2, len(self.lines)):
            line = self.lines[i]
            # AocLogger.log(line)
            self.instructions.append(aoc_util.ints(line))
            self.instructions_raw.append(line)

    def parse_line(self, line):
        """
        [Z] [M] [P]

        stack 0: c1
        stack 1: c5
        stack 2: c9
        """
        # AocLogger.log(line)
        for i in range(self.num_stacks):
            col = 4 * i + 1
            try:
                letter = line[col]
            except:
                letter = ' '
            # AocLogger.log(letter)
            if letter.isalpha():
                self.stacks[i].append(letter)

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def show_stacks(self, when, num_moves, stack_num):
        i = 0
        for stack in self.stacks:
            i += 1
            text = '|'.join(stack)
            text = '{}: {}'.format(i, text)
            if i == stack_num:
                text += ' '
                dashes = '-' * num_moves
                if when == 'after':
                    text += '<'
                text += dashes
                if when == 'before':
                    text += '>'
            AocLogger.log(text)
        AocLogger.log()

    def p1(self):
        """
        crates are moved one at a time
        """
        for x in range(len(self.instructions)):
            num_moves, src, dest = self.instructions[x]

            AocLogger.log('\n\ninstruction {}: ({})'.format(x + 1, self.instructions_raw[x]))
            self.show_stacks('before', num_moves, src)

            # move crates one at a time (todo: extract into method)
            for y in range(num_moves):
                crate = self.stacks[src - 1].pop()
                self.stacks[dest - 1].append(crate)

            self.show_stacks('after', num_moves, dest)

        result = ''
        for stack in self.stacks:
            result += stack[-1]
        return result

    def p2(self):
        """
        same, except the crates are moved as a group
        """
        for x in range(len(self.instructions)):
            num_moves, src, dest = self.instructions[x]

            AocLogger.log('\ninstruction {}: ({})'.format(x + 1, self.instructions_raw[x]))
            self.show_stacks('before', num_moves, src)

            # move all at once (todo: extract into method)
            src_stack = self.stacks[src - 1]
            crates = src_stack[-num_moves:]
            self.stacks[src - 1] = src_stack[:-num_moves]  # remove from src
            self.stacks[dest - 1] += crates  # add to dest

            self.show_stacks('after', num_moves, dest)

        result = ''
        for stack in self.stacks:
            result += stack[-1]
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
