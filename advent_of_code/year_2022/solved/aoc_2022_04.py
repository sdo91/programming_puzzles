#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    2,
    0,
    0,
]

TEST_OUTPUT_2 = [
    4,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            490,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            921,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        for line in self.lines:
            min_a, max_a, min_b, max_b = aoc_util.positive_ints(line)

            if min_a >= min_b and max_a <= max_b:
                result += 1  # A is inside B
            elif min_b >= min_a and max_b <= max_a:
                result += 1  # B is inside A

        return result

    def is_overlap_p2(self, range_a, range_b):
        """
        true if either bound of A is inside B
        """
        min_a, max_a = aoc_util.positive_ints(range_a)
        min_b, max_b = aoc_util.positive_ints(range_b)

        if min_b <= min_a <= max_b:
            return True
        elif min_b <= max_a <= max_b:
            return True
        else:
            return False

    def p2(self):
        result = 0
        for line in self.lines:
            range_a, range_b = line.split(',')
            if self.is_overlap_p2(range_a, range_b) or self.is_overlap_p2(range_b, range_a):
                result += 1
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
