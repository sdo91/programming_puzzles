#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    24,
    None,
    None,
]

TEST_OUTPUT_2 = [
    93,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            618,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            26358,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):
    start_coord = (500, 0)

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        self.grid = self.parse(self.lines)
        if AocLogger.verbose:
            self.grid.show()
        self.bottom = self.grid.max_y

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    @classmethod
    def parse(cls, lines):
        grid = Grid2D(default='.')
        grid.set_tuple(cls.start_coord, '+')
        for line in lines:
            prev_coord = None
            for token in line.split(' -> '):
                coord = tuple(aoc_util.ints(token))
                if prev_coord:
                    grid.set_range(prev_coord, coord, '#')
                prev_coord = coord
        return grid

    def p1(self):
        result = 0
        while self.drop_sand():
            result += 1

        self.grid.show()
        return result

    def drop_sand(self, p1=True):
        current_coord = self.start_coord

        while True:
            # check if done
            if p1:
                if current_coord[1] > self.bottom:
                    return None
            else:  # p2
                if current_coord[1] == self.bottom + 1:
                    self.grid.set_tuple(current_coord, 'o')
                    return current_coord

            # try down
            down = Grid2D.get_coord_south(current_coord)
            if self.grid.is_value(down, '.'):
                current_coord = down
                continue

            # try down/left
            down_left = Grid2D.get_coord_sw(current_coord)
            if self.grid.is_value(down_left, '.'):
                current_coord = down_left
                continue

            # try down/right
            down_right = Grid2D.get_coord_se(current_coord)
            if self.grid.is_value(down_right, '.'):
                current_coord = down_right
                continue

            # stuck
            self.grid.set_tuple(current_coord, 'o')
            return current_coord

    def p2(self):
        result = 0
        while True:
            placed_coord = self.drop_sand(p1=False)
            result += 1
            if placed_coord == self.start_coord:
                break

        self.grid.show()
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
