#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
30373
25512
65332
33549
35390
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    21,
    None,
    None,
]

TEST_OUTPUT_2 = [
    8,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            1785,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            345168,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.grid = Grid2D(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for coord in self.grid.coords():
            if self.is_visible(coord):
                AocLogger.log("{} is visible".format(coord))
                result += 1
            else:
                AocLogger.log("{} is NOT visible".format(coord))
        return result

    def is_visible(self, start_coord):
        """
        is coord visible from outside?
        algo:
        go [up down left right]
        if all are shorter and we get to edge: visible
        on one direction needs to be visible
        """
        height_a = self.grid.get_int(start_coord)
        for direction in Grid2D.DIRECTIONS.keys():
            is_blocked = False
            next_tree_coord = start_coord
            while not is_blocked:
                next_tree_coord = Grid2D.get_coord_direction(next_tree_coord, direction)
                if self.grid.is_out_of_bounds(next_tree_coord):
                    return True  # visible
                height_b = self.grid.get_int(next_tree_coord)
                if height_b >= height_a:
                    is_blocked = True
        return False

    def p2(self):
        result = 0
        for coord in self.grid.coords():
            scenic_score = self.calc_scenic_score(coord)
            result = max(result, scenic_score)
        return result

    def calc_scenic_score(self, start_coord):
        """
        In the example above, consider the middle 5 in the second row:

        30373
        25512
        65332
        33549
        35390

        Looking up, its view is not blocked; it can see 1 tree (of height 3).
        Looking left, its view is blocked immediately; it can see only 1 tree (of height 5, right next to it).
        Looking right, its view is not blocked; it can see 2 trees.
        Looking down, its view is blocked eventually; it can see 2 trees (one of height 3, then the tree of height 5 that blocks its view).
        A tree's scenic score is found by multiplying together its viewing distance in each of the four directions. For this tree, this is 4 (found by multiplying 1 * 1 * 2 * 2).
        """
        height_a = self.grid.get_int(start_coord)
        scores = []
        for direction in Grid2D.DIRECTIONS.keys():
            scores.append(0)
            is_blocked = False
            next_tree_coord = start_coord
            while not is_blocked:
                next_tree_coord = Grid2D.get_coord_direction(next_tree_coord, direction)
                if self.grid.is_out_of_bounds(next_tree_coord):
                    break  # edge
                scores[-1] += 1
                height_b = self.grid.get_int(next_tree_coord)
                if height_b >= height_a:
                    is_blocked = True

        result = 1
        for score in scores:
            result *= score
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
