#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    13140,
    None,
    None,
]

TEST_OUTPUT_2 = [
    'grid',
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            15140,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            'grid',
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        # p1
        self.register = 1
        self.cycle = 0
        self.signal_strength = 0

        # p2
        self.grid = Grid2D()
        self.grid.set_value_width(2)

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        addx V takes two cycles to complete. After two cycles, the X register is increased by the value V.
            (V can be negative.)
        noop takes one cycle to complete. It has no other effect.
        """
        for line in self.lines:
            if 'addx' in line:
                value = int(line.split()[-1])
                self.check_signal()
                self.check_signal()
                self.register += value
            elif 'noop' in line:
                self.check_signal()
            else:
                assert False

        return self.signal_strength

    def check_signal(self):
        """
        Maybe you can learn something by looking at the value of the X register throughout execution.
        For now, consider the signal strength (the cycle number multiplied by the value of the X register)
            during the 20th cycle and every 40 cycles after that
            (that is, during the 20th, 60th, 100th, 140th, 180th, and 220th cycles).
        """
        self.cycle += 1
        if (self.cycle - 20) % 40:
            return
        if self.cycle >= 260:
            assert False  # todo: handle?
        signal = self.cycle * self.register
        self.signal_strength += signal

    def p2(self):
        for line in self.lines:
            if 'addx' in line:
                value = int(line.split()[-1])
                self.render()
                self.render()
                self.register += value
            elif 'noop' in line:
                self.render()
            else:
                assert False

        self.grid.show()
        return 'grid'

    def render(self):
        y = self.cycle // 40
        x = self.cycle % 40

        # is sprite visible?
        diff = abs(x - self.register)
        is_visible = (diff <= 1)

        if is_visible:
            self.grid.set_tuple((x, y), '##')

        self.cycle += 1


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
