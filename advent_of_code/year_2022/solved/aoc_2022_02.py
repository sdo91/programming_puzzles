#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
A Y
B X
C Z
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    15,
    0,
    0,
]

TEST_OUTPUT_2 = [
    12,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            10718,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            14652,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_move(self, move):
        if move in {'A', 'X'}:
            return 1
        elif move in {'B', 'Y'}:
            return 2
        else:
            return 3

    def calc_score(self, them, me):
        diff = (me - them) % 3
        if diff == 1:
            return me + 6  # win
        if diff == 0:
            return me + 3  # draw
        else:
            return me  # loss

    def score_games(self, games):
        score = 0
        for game in games:
            score += self.calc_score(game[0], game[1])
        return score

    def p1(self):
        """

        """
        games = []
        for line in self.lines:
            them, me = line.split()
            games.append((self.parse_move(them), self.parse_move(me)))

        return self.score_games(games)

    def parse_my_move_p2(self, move, them_parsed):
        if move == 'X':
            result = them_parsed - 1  # need loss
        elif move == 'Y':
            result = them_parsed  # need draw
        else:
            result = them_parsed + 1  # need win

        # wrap my move to be between 1 and 3
        if result < 1:
            result += 3
        if result > 3:
            result -= 3
        return result

    def p2(self):
        """
        need to parse my move differently from p1, but calc the scores the same way
        """
        games = []
        for line in self.lines:
            them, me = line.split()
            them_parsed = self.parse_move(them)
            games.append((them_parsed, self.parse_my_move_p2(me, them_parsed)))

        return self.score_games(games)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
