#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

from functools import cmp_to_key
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    13,
    None,
    None,
]

TEST_OUTPUT_2 = [
    140,
    None,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            5330,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            27648,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        pair_index = 1
        pairs = self.text.split('\n\n')

        for pair in pairs:
            lines = aoc_util.lines(pair)
            # left, right = map(aoc_util.recursive_parse_list, lines)
            left, right = map(eval, lines)

            are_in_order = (self.compare_order(left, right) < 0)

            AocLogger.log('\npair {}'.format(pair_index))
            AocLogger.log(left)
            AocLogger.log(right)
            AocLogger.log('are_in_order: {}'.format(are_in_order))

            if are_in_order:
                result += pair_index
            pair_index += 1

        return result

    def compare_order(self, left, right):
        """
        If both values are integers, the lower integer should come first. If the left integer is lower than the right
        integer, the inputs are in the right order. If the left integer is higher than the right integer, the inputs
        are not in the right order. Otherwise, the inputs are the same integer; continue checking the next part of
        the input.

        If both values are lists, compare the first value of each list, then the second value, and so on. If the left
        list runs out of items first, the inputs are in the right order. If the right list runs out of items first,
        the inputs are not in the right order. If the lists are the same length and no comparison makes a decision
        about the order, continue checking the next part of the input.

        If exactly one value is an integer, convert the integer to a list which contains that integer as its only
        value, then retry the comparison. For example, if comparing [0,0,0] and 2, convert the right value to [2] (a
        list containing 2); the result is then found by instead comparing [0,0,0] and [2].

        Returns:
            int: 1 if right is bigger, -1 if left is bigger, 0 if same
        """
        num_ints = sum([type(left) == int, type(right) == int])
        if num_ints == 2:
            diff = left - right
            return aoc_util.sign(diff)

        # make sure both are lists
        left = self.to_list(left)
        right = self.to_list(right)

        # compare
        i = 0
        while True:
            if i == len(left) and i == len(right):
                return 0  # lists equal
            elif i == len(left):
                # If the left list runs out of items first, the inputs are in the right order
                return -1
            elif i == len(right):
                # If the right list runs out of items first, the inputs are not in the right order
                return 1
            else:
                # compare the item
                comparison = self.compare_order(left[i], right[i])
                if comparison != 0:
                    return comparison
            i += 1

    @classmethod
    def to_list(cls, i):
        if type(i) == int:
            return [i]
        else:
            return i

    def p2(self):
        divider_2 = [[2]]
        divider_6 = [[6]]

        text = self.text.replace('\n\n', '\n')
        lines = aoc_util.lines(text)

        packets = []
        for line in lines:
            packets.append(eval(line))
        packets.append(divider_2)
        packets.append(divider_6)

        packets = sorted(packets, key=cmp_to_key(self.compare_order))

        i2 = packets.index(divider_2) + 1
        i6 = packets.index(divider_6) + 1
        return i2 * i6


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
