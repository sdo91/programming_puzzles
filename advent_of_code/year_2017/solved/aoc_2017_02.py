#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
import itertools

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
5 1 9 5
7 5 3
2 4 6 8
    """, """
5 9 2 8
9 4 7 3
3 8 6 5
    """, """

    """
]

TEST_OUTPUT_1 = [
    18,
    None,
    None,
]

TEST_OUTPUT_2 = [
    None,
    9,
    None,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            45972,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            0,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        For each row, determine the difference between the largest value and the smallest value;
        the checksum is the sum of all of these differences.
        """
        result = 0
        for line in self.lines:
            values = aoc_util.ints(line)
            diff = max(values) - min(values)
            result += diff
        return result

    def p2(self):
        """
        It sounds like the goal is to find the only two numbers in each row where one evenly divides the other -
        that is, where the result of the division operation is a whole number.
        They would like you to find those numbers on each line, divide them, and add up each line's result.
        """
        result = 0
        for line in self.lines:
            values = aoc_util.ints(line)
            combos = itertools.combinations(values, 2)
            for combo in combos:
                a, b = max(combo), min(combo)
                if a % b == 0:
                    # found it
                    result += (a // b)
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
