#!/usr/bin/env python3


class BinarySearchResult(object):
    def __init__(self, start_result, low, high):
        if start_result is True:
            self.max_true = low
            self.min_false = high
        else:
            self.max_false = low
            self.min_true = high


class BinarySearchUtil(object):

    @staticmethod
    def binary_search(func, start_value):
        """
        Args:
            func (function(int) -> bool)):
            start_value (int): starting lower bound

        Returns:
            BinarySearchResult:
        """
        start_result = func(start_value)  # type: bool
        low = start_value
        high = low + 1

        # quickly calculate upper/lower bounds
        while func(high) == start_result:
            low = high
            high *= 2
        # assert func(low) == start_result
        # assert func(high) != start_result

        # do binary search
        while True:
            if high == low + 1:
                return BinarySearchResult(start_result, low, high)

            midpoint = (low + high) // 2
            if func(midpoint) == start_result:
                low = midpoint
            else:
                high = midpoint
