#!/usr/bin/env python3

import math
import pprint


class AocLogger(object):
    verbose = True

    @classmethod
    def log(cls, msg=None):
        if cls.verbose:
            if msg is None:
                print()
            else:
                print(msg)

    @classmethod
    def log_dict(cls, my_dict, name='', force_verbose=False):
        if cls.verbose or force_verbose:
            if name:
                print('{}:'.format(name))
            pprint.pprint(my_dict)
            # print(json.dumps(my_dict, indent=2))
            print()

    @classmethod
    def log_percent(cls, i, total_size, every_n_percent=1, end='\n'):
        """
        Args:
            i (int): current index
            total_size: usually an int, can also be a container with len()
            every_n_percent (number):
            end (str):
        """
        if type(total_size) == float:
            total_size = int(total_size)
        if type(total_size) != int:
            total_size = len(total_size)
        chunk_size = math.ceil(total_size * every_n_percent / 100.0)
        if chunk_size < 1 or i % chunk_size == 0:
            percent = 100 * i / total_size
            print('{:.1f}% (i={})'.format(percent, i), end=end)
