#!/usr/bin/env python3

from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.dijkstra_node import DijkstraNode
from advent_of_code.util.grid_2d import Grid2D
from advent_of_code.util.min_heap import MinHeap


class DijkstraSolver(object):
    """
    usage:
    write a class that inherits from this one

    examples:
    CaveSolver
    ChitonSolver
    HeightmapPathfinder
    """

    def __init__(self, grid, open_chars, goal_chars):
        self.grid = grid  # type: Grid2D

        # chars we can pass through
        self.open_chars = open_chars  # type: set

        # we are done if we get to one of these (can be empty)
        self.goal_chars = goal_chars  # type: set

        # to keep track of what we have logged
        self.log_tracker = 0

    def is_done(self, coord):
        """
        return true if we are done (ie: best path found)
        """
        char_at_coord = self.grid.get_top(coord)
        return char_at_coord in self.goal_chars

    def get_adjacent(self, coord):
        """
        return list of coords that are adjacent
        """
        return self.grid.get_adjacent_coords(coord)

    def can_reach(self, coord):
        """
        return true if the given coord can be passed through
        (eg: not OOB, not a wall, etc)
        """
        char_at_coord = self.grid.get_top(coord)
        return char_at_coord in self.open_chars or char_at_coord in self.goal_chars

    def distance(self, first_coord, second_coord):
        """
        return the cost to travel from first_coord to second_coord
        """
        return 1

    def check_progress(self, selected_node: DijkstraNode):
        """
        log distance every ~100 units
        """
        # print('\nselected_node: {}'.format(selected_node))
        new_tracker = selected_node.dist // 100
        if new_tracker > self.log_tracker:
            self.log_tracker = new_tracker
            print('distance so far: {}'.format(selected_node.dist))

    def find_shortest_path(self, start_coord=(0, 0), max_path_length=None) -> DijkstraNode:
        """
        Args:
            start_coord (tuple):
            max_path_length (int): fail once we exceed this

        Returns:
            DijkstraNode: the final node, with distance and path

        given:
            start coord
            open chars (can path through)
            goal chars (done when we hit)

        assume:
            cant move diag

        algo:
            keep a pq of nodes (just starts with one)

            while pq:
                select node at shortest dist (reading order)
                mark as visited
                check if done (goal reached)
                update dist to all reachable nodes
        """

        priority_queue = MinHeap()  # unvisited

        start_node = DijkstraNode(start_coord)
        start_node.dist = 0
        # start_node.path.append(start_coord)

        all_nodes = {
            start_coord: DijkstraNode(start_coord)
        }

        priority_queue.insert(start_node, 0)

        # while queue not empty
        while priority_queue:

            # select node at shortest distance
            selected_node = priority_queue.pop()  # type: DijkstraNode
            self.check_progress(selected_node)

            # check if done
            if self.is_done(selected_node.coord):
                return selected_node

            # check if we have exceeded max_path_length
            if max_path_length:
                if len(selected_node.path) > max_path_length:
                    AocLogger.log('path too long: {}'.format(len(selected_node.path)))
                    start_node.path = None
                    return start_node  # no path found

            # update dist to reachable nodes
            for adj_coord in self.get_adjacent(selected_node.coord):

                if self.can_reach(adj_coord):
                    # coord is valid

                    # create the potential new node
                    new_node = DijkstraNode(adj_coord)
                    new_node.dist = selected_node.dist + self.distance(selected_node.coord, adj_coord)
                    new_node.path = selected_node.path.copy()
                    new_node.path.append(adj_coord)

                    # check if the new node is better
                    if adj_coord in all_nodes:
                        prev_node = all_nodes[adj_coord]
                        if not new_node.is_better_than(prev_node):
                            # skip since the new node is not better
                            continue

                    # add node (node is new, or better than old)
                    all_nodes[adj_coord] = new_node
                    priority_queue.insert(new_node, new_node.dist)

        # if we get here, no path was found
        start_node.path = None
        return start_node
