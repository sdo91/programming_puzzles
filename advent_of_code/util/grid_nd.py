#!/usr/bin/env python3

import typing
from collections import defaultdict
from advent_of_code.util.aoc_util import AocLogger


class GridND(object):
    LARGE = 999999999

    def __init__(self, num_dimensions, default=' '):
        """
        todo:
        min max
        entries

        Args:
            num_dimensions:
        """
        self.num_dimensions = num_dimensions
        self.grid = defaultdict(lambda: default)
        self.min_values = [self.LARGE] * num_dimensions
        self.max_values = [-self.LARGE] * num_dimensions

        AocLogger.log('GridND')

    def check_coord(self, coord):
        if len(coord) != self.num_dimensions:
            assert False

    def set(self, coord, value):
        self.check_coord(coord)
        self.grid[coord] = value

        for i in range(self.num_dimensions):
            if coord[i] < self.min_values[i]:
                self.min_values[i] = coord[i]
            if coord[i] > self.max_values[i]:
                self.max_values[i] = coord[i]

    def get(self, coord):
        self.check_coord(coord)
        return self.grid[coord]
