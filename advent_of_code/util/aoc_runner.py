#!/usr/bin/env python3

import aocd
import os
import sys
import time
import traceback

from advent_of_code.util.aoc_util import AocUtil
from advent_of_code.util.aoc_logger import AocLogger


class AocRunner(object):

    def __init__(self, solver_class: type, filename: str):
        print('starting {}'.format(__file__.split('/')[-1]))

        self.solver_class = solver_class

        self.should_run_p1_tests = True

        try:
            basename = os.path.basename(filename)
            year, day = AocUtil.ints(basename)
            self.puzzle_input = aocd.get_data(day=day, year=year)
            # self.puzzle_input = aocd.get_data(day=day, year=year, block=True)
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        AocUtil.write_input(self.puzzle_input, __file__)

    def run_tests(self, test_inputs, test_outputs_p1, test_outputs_p2):
        self.solver_class.is_test = True
        AocLogger.verbose = True

        if self.should_run_p1_tests:
            AocUtil.run_tests(self.solve_part_1, test_inputs, test_outputs_p1)
        AocUtil.run_tests(self.solve_part_2, test_inputs, test_outputs_p2)

    def run(self, real_outputs: list):
        start_time = time.time()

        self.solver_class.is_test = False
        AocLogger.verbose = False

        p1_out, p2_out = real_outputs

        # do part 1
        should_skip_p1 = (p1_out and not p2_out)  # skip p1 if working on p2 (p1 valid, p2 not valid)
        if not should_skip_p1:
            p1_result = self.solve_part_1(self.puzzle_input)
            if p1_out is None:
                print("SUBMIT P1: {}".format(p1_result))
                sys.exit(1)

            AocUtil.assert_equal(p1_out, p1_result)

        # do part 2
        p2_result = self.solve_part_2(self.puzzle_input)
        if p2_out is None:
            print("SUBMIT P2: {}".format(p2_result))
            sys.exit(1)

        AocUtil.assert_equal(
            p2_out,
            p2_result
        )

        # show time
        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def solve_part_1(self, text: str):
        solver = self.solver_class(text, 1)

        part_1_result = solver.p1()

        print('part_1_result:\n{}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = self.solver_class(text, 2)

        part_2_result = solver.p2()

        print('part_2_result:\n{}\n'.format(part_2_result))
        return part_2_result

    def skip_p1_tests(self):
        self.should_run_p1_tests = False
