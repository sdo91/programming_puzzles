#!/usr/bin/env python3

import re
import time
import typing
from collections import defaultdict
from os import path

from advent_of_code.util.aoc_logger import AocLogger


class AocUtil(object):

    @classmethod
    def lmap(cls, func, *iterables):
        return list(map(func, *iterables))

    @classmethod
    def ints(cls, s: str) -> typing.List[int]:
        return cls.lmap(int, re.findall(r"-?\d+", s))  # thanks mserrano!

    @classmethod
    def parse_only_int(cls, s: str) -> int:
        return cls.ints(s)[0]

    @classmethod
    def positive_ints(cls, s: str) -> typing.List[int]:
        return cls.lmap(int, re.findall(r"\d+", s))  # thanks mserrano!

    @classmethod
    def floats(cls, s: str) -> typing.List[float]:
        return cls.lmap(float, re.findall(r"-?\d+(?:\.\d+)?", s))

    @classmethod
    def positive_floats(cls, s: str) -> typing.List[float]:
        return cls.lmap(float, re.findall(r"\d+(?:\.\d+)?", s))

    @classmethod
    def words(cls, s: str) -> typing.List[str]:
        return re.findall(r"[a-zA-Z]+", s)

    @classmethod
    def lines(cls, s: str) -> typing.List[str]:
        return s.split('\n')

    @classmethod
    def stripped_lines(cls, s: str) -> typing.List[str]:
        return cls.split_and_strip_each(s.strip(), '\n')

    @classmethod
    def split_and_strip_each(cls, string: str, delim: str) -> typing.List[str]:
        """
        split the string on the char, and strip each item in the resulting list
        """
        return cls.lmap(str.strip, string.split(delim))

    @classmethod
    def assert_equal(cls, expected, actual):
        if expected != actual:
            print('expected: {}'.format(expected))
            print('actual: {}'.format(actual))
            time.sleep(0.1)
        assert expected == actual

    @classmethod
    def write_input(cls, text, file_path):
        out_dir = path.dirname(file_path)
        while not path.basename(out_dir).startswith('advent'):
            out_dir = path.realpath(path.join(out_dir, '..'))

        with open(out_dir + '/input.txt', 'w') as outfile:
            outfile.write(text)

    @classmethod
    def run_tests(cls, function, test_inputs: typing.List[str], test_outputs, cases=None):
        """
        Args:
            function (func): to be tested
            test_inputs (list): to be passed in
            test_outputs (list): if None, test will be skipped
            cases (set[int]):
        """
        AocLogger.log('\nrunning test cases ({})'.format(str(function)))
        num_tests_passed = 0

        for i in cls.range_len(test_inputs):
            if cases and i not in cases:
                print('skipping test case: {}'.format(i))
                continue
            test_in = test_inputs[i]
            test_in = test_in.strip('\n')  # todo: will this ever cause issues?
            try:
                test_out = test_outputs[i]
            except IndexError:
                test_out = None

            if test_in and type(test_in) == str:
                test_in = test_in.strip()
                if test_in == '':
                    continue

            if not test_in or test_out is None:
                print('skipping test case: {} (test_in={}, test_out={})'.format(i, test_in, test_out))
                continue

            print('\ntest case: {}'.format(i))

            # do the test
            cls.assert_equal(
                test_out,
                function(test_in)
            )

            num_tests_passed += 1

        AocLogger.log('\npassed all test cases ({} tests)'.format(num_tests_passed))
        AocLogger.log('\n' * 5)

    @classmethod
    def manhatten_dist(cls, a, b=(0, 0, 0)):
        result = 0
        for i in cls.range_len(a):
            result += abs(a[i] - b[i])
        return result

    @classmethod
    def re_find_all_matches(cls, pattern, text):
        matcher = re.compile(pattern)
        return [match.group() for match in matcher.finditer(text)]

    @classmethod
    def re_find_all_positions(cls, pattern, text):
        matcher = re.compile(pattern)
        result = []
        for match in matcher.finditer(text):
            pos = match.span()[0]
            result.append(pos)
        return result

    @classmethod
    def re_is_match(cls, pattern, text):
        return bool(re.search(pattern, text))

    @classmethod
    def tuple_add(cls, a, b):
        return tuple(i + j for i, j in zip(a, b))

    @classmethod
    def tuple_subtract(cls, a, b):
        return tuple(i - j for i, j in zip(a, b))

    @classmethod
    def format_coords(cls, tup: tuple):
        return ''.join([x for x in str(tup) if x.isdigit() or x == ','])

    @classmethod
    def join_ints(cls, lst):
        return ''.join([str(x) for x in lst])

    @classmethod
    def digits(cls, obj):
        result = []
        for char in str(obj):
            try:
                result.append(int(char))
            except:
                pass
        return result

    @classmethod
    def is_even(cls, x):
        return x % 2 == 0

    @classmethod
    def is_odd(cls, x):
        return x % 2 != 0

    @classmethod
    def sign(cls, x):
        if x > 0:
            return 1
        elif x < 0:
            return -1
        else:
            return 0

    @classmethod
    def is_reading_order(cls, a, b):
        """
        When multiple choices are equally valid, ties are broken in reading order: top-to-bottom, then left-to-right.

        Args:
            a:
            b:

        Returns:

        """
        if a[1] == b[1]:
            return a[0] < b[0]  # if same row, choose left
        else:
            return a[1] < b[1]  # choose top

    @classmethod
    def range_len(cls, x):
        return range(len(x))

    @classmethod
    def tokenize(cls, text, delimiters=' \t\n'):
        result = []
        start_index = 0
        chars_to_keep = 0
        for x in range(len(text) + 1):
            if x >= len(text) or text[x] in delimiters:
                # save the token
                if chars_to_keep:
                    token = text[start_index:x]
                    token = cls.to_number_if_possible(token)
                    result.append(token)
                # reset
                start_index = x + 1
                chars_to_keep = 0
            else:
                # keep the char
                chars_to_keep += 1

        return result

    @classmethod
    def to_number_if_possible(cls, x):
        try:
            return int(x)
        except ValueError:
            try:
                return float(x)
            except ValueError:
                return x

    @classmethod
    def has_duplicates(cls, collection):
        all = set()
        for item in collection:
            if item in all:
                return True
            all.add(item)
        assert len(all) == len(collection)
        return False

    @classmethod
    def rotate_left(cls, my_list, n):
        """
        rotate list left by n
        """
        return my_list[n:] + my_list[:n]

    @classmethod
    def remove_chars(cls, string, chars):
        """
        remove all chars from string
        """
        for c in chars:
            string = string.replace(c, '')
        return string

    @classmethod
    def binary_str_to_int(cls, binary_string: str) -> int:
        return int(binary_string, 2)

    @classmethod
    def int_to_binary_str(cls, i: int) -> str:
        return bin(i)[2:]

    @classmethod
    def get_single_value_from_set(cls, my_set: set):
        assert len(my_set) == 1
        return list(my_set)[0]

    @classmethod
    def count(cls, iterable):
        counts = defaultdict(int)
        for x in iterable:
            counts[x] += 1
        return counts

    @classmethod
    def parse_dict(cls, text, entry_delimiter=",", kvp_delimiter=":", func_to_apply=None):
        text = text.strip()
        result = {}
        for entry in text.split(entry_delimiter):
            key, value = cls.split_and_strip_each(entry, kvp_delimiter)
            if func_to_apply is not None:
                value = func_to_apply(value)
            result[key] = value
        return result
