#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
H => HO
H => OH
O => HH

HOH
    """, """
H => HO
H => OH
O => HH

HOHOHO
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    4,
    7,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    509,
    None
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.replacements_text, self.start_molecule = AocUtil.split_and_strip_each(text, "\n\n")

        self.replacements = []
        for line in AocUtil.lines(self.replacements_text):
            key, value = AocUtil.split_and_strip_each(line, "=>")
            self.replacements.append((key, value))

        self.distinct_molecules = set()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        for pair in self.replacements:
            self.process_replacement(*pair)

        return len(self.distinct_molecules)

    def process_replacement(self, key, value):
        key_indexes = self.find_all_substrings(self.start_molecule, key)
        for key_index in key_indexes:
            new_molecule = self.do_replacement(self.start_molecule, key, value, key_index)
            self.distinct_molecules.add(new_molecule)

    @staticmethod
    def do_replacement(molecule, key, value, key_index):
        prefix = molecule[:key_index]
        suffix = molecule[key_index + len(key):]
        return prefix + value + suffix

    @staticmethod
    def find_all_substrings(string, substring):
        """
        Finds all occurrences of a substring in a string and returns their starting indices.

        Args:
          string: The string to search in.
          substring: The substring to search for.

        Returns:
          A list of the starting indices of all occurrences of the substring in the string.
        """
        indices = []
        index = string.find(substring)
        while index != -1:
            indices.append(index)
            index = string.find(substring, index + 1)
        return indices

    def p2(self):
        result = 0
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
