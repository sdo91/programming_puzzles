#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
150
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    8,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    665280,
    None
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.target_number = int(text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        best = 0
        if self.is_test:
            houses = range(1, self.target_number)
        else:
            houses = range(20, self.target_number, 20)
        for house_number in houses:
            AocLogger.log_percent(house_number, 1e6)
            num_presents = self.calc_num_presents(house_number)
            AocLogger.log("House {} got {} presents".format(house_number, num_presents))
            if num_presents > best:
                best = num_presents
                print("House {} got {} presents (ratio={:.2f})".format(
                    house_number, num_presents, num_presents / house_number))
            if num_presents >= self.target_number:
                factors = self.get_all_factors(house_number)
                print("factors: {}".format(factors))
                return house_number
        return -1

    @classmethod
    def calc_num_presents(cls, house_number: int) -> int:
        factors = cls.get_all_factors(house_number)
        result = 10 * sum(factors)
        return result

    @classmethod
    def get_prime_factors(cls, n):
        """
        Calculates the prime factors of a given number.

        Args:
          n: The number for which to find prime factors.

        Returns:
          A list of prime factors for the given number.
        """
        i = 2
        factors = []
        while n > 1:
            if n % i == 0:
                factors.append(i)
                n //= i
            else:
                i += 1
        return factors

    @classmethod
    def get_all_factors(cls, n):
        factors = {1}
        prime_factors = cls.get_prime_factors(n)
        for p in prime_factors:
            factors |= {f * p for f in factors}
        return sorted(factors)

    def p2(self):
        result = 0
        return result

    @classmethod
    def run_unit_tests(cls):
        factors = cls.get_all_factors(144)
        assert 15 == len(factors)

        assert 150 == cls.calc_num_presents(8)
        assert 130 == cls.calc_num_presents(9)


if __name__ == '__main__':
    Solver.run_unit_tests()
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
