#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    62842880,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    57600000,
    None,
    None,
]

REAL_OUTPUTS = [
    18965440,
    15862900
]


class Ingredient(object):
    def __init__(self, text):
        self.text = text
        self.name = text.split(':')[0].strip()
        self.property_scores = AocUtil.ints(text)

    def __repr__(self):
        return self.text

    def calc_subscore(self, spoons_of_i, property_id):
        prop_score_of_i = self.property_scores[property_id]
        return spoons_of_i * prop_score_of_i


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.ingredients = self.parse_input()

        AocLogger.log(str(self))

    def parse_input(self):
        lines = AocUtil.lines(self.text)
        ingredients = []
        for line in lines:
            ingredients.append(Ingredient(line))
        return ingredients

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        if self.is_test:
            generator = self.generate_mixes_of_2(100)
        else:
            generator = self.generate_mixes_of_4(100)
        for potential_mix in generator:
            score = self.score_cookie(potential_mix)
            result = max(result, score)
        return result

    def score_cookie(self, potential_mix):
        if self.part == 2:
            if not self.has_calories(potential_mix, 500):
                return 0
        cookie_score = 1
        for property_id in range(4):
            property_score = self.score_property(potential_mix, property_id)
            cookie_score *= property_score
        return cookie_score

    def score_property(self, potential_mix, property_id):
        property_score = 0
        for ingredient_id in range(len(self.ingredients)):
            spoons_of_i = potential_mix[ingredient_id]
            ingredient = self.ingredients[ingredient_id]
            property_score += ingredient.calc_subscore(spoons_of_i, property_id)
        property_score = max(0, property_score)
        return property_score

    def p2(self):
        return self.p1()

    def has_calories(self, potential_mix, num_calories):
        property_id = 4
        score = self.score_property(potential_mix, property_id)
        return num_calories == score

    @staticmethod
    def generate_mixes_of_2(size):
        for a in range(size + 1):
            b = size - a
            yield a, b

    @classmethod
    def generate_mixes_of_4(cls, size):
        for sub_mix in cls.generate_mixes_of_2(size):
            ab_size, cd_size = sub_mix
            for mix_ab in cls.generate_mixes_of_2(ab_size):
                for mix_cd in cls.generate_mixes_of_2(cd_size):
                    full_mix = (mix_ab[0], mix_ab[1], mix_cd[0], mix_cd[1])
                    yield full_mix

    @classmethod
    def run_unit_tests(cls):
        # tests for 2 ingredients
        mixes = list(cls.generate_mixes_of_2(0))
        assert len(mixes) == 1

        mixes = list(cls.generate_mixes_of_2(1))
        assert len(mixes) == 2

        mixes = list(cls.generate_mixes_of_2(2))
        assert len(mixes) == 3

        mixes = list(cls.generate_mixes_of_2(3))
        assert len(mixes) == 4

        # tests for 4 ingredients
        mixes = list(cls.generate_mixes_of_4(0))
        assert len(mixes) == 1

        mixes = list(cls.generate_mixes_of_4(1))
        assert len(mixes) == 4

        mixes = list(cls.generate_mixes_of_4(2))
        assert len(mixes) == 10


if __name__ == '__main__':
    Solver.run_unit_tests()
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
