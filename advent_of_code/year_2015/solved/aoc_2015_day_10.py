#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
1
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    312211,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    492982,
    6989950
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def get_num_iterations(self):
        if self.is_test:
            return 5
        elif self.part == 1:
            return 40
        else:
            return 50

    def p1(self):
        before = self.text
        num_iterations = self.get_num_iterations()
        for i in range(num_iterations):
            AocLogger.log_percent(i, num_iterations)
            after = self.process(before)
            AocLogger.log("{} becomes {}".format(before, after))
            before = after

        if self.is_test:
            return int(after)
        else:
            return len(after)

    def p2(self):
        return self.p1()

    def process(self, input_str):
        result = ""

        i = 0
        while i < len(input_str):
            num_digits = self.count_digits(input_str, i)
            digit = input_str[i]

            result += str(num_digits) + digit

            i += num_digits

        return result

    @staticmethod
    def count_digits(input_str, start_index):
        digit = input_str[start_index]
        result = 0
        for i in range(start_index, len(input_str)):
            if input_str[i] == digit:
                result += 1
            else:
                break
        return result

    @classmethod
    def run_unit_tests(cls):
        assert cls.count_digits("312211", 0) == 1
        assert cls.count_digits("312211", 1) == 1
        assert cls.count_digits("312211", 2) == 2
        assert cls.count_digits("312211", 4) == 2


if __name__ == '__main__':
    Solver.run_unit_tests()
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
