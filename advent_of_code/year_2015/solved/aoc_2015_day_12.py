#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import json

### CONSTANTS ###
TEST_INPUTS = [
    """
[1,{"c":"red","b":2},3]
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    6,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    4,
    None,
    None,
]

REAL_OUTPUTS = [
    191164,
    87842
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        return sum(AocUtil.ints(self.text))

    def p2(self):
        json_obj = json.loads(self.text)

        json_obj = self.recursive_remove_red(json_obj)

        return sum(AocUtil.ints(json.dumps(json_obj)))

    def recursive_remove_red(self, json_obj):
        if type(json_obj) is list:
            for i in range(len(json_obj)):
                json_obj[i] = self.recursive_remove_red(json_obj[i])
        if type(json_obj) is dict:
            for key in json_obj.keys():
                old_value = json_obj[key]
                if old_value == "red":
                    return 0
                json_obj[key] = self.recursive_remove_red(old_value)
        return json_obj


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
