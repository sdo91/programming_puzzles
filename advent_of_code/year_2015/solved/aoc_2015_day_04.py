#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger

import hashlib

### CONSTANTS ###
TEST_INPUTS = [
    """
abcdef
    """, """
pqrstuv
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    609043,
    1048970,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    117946,
    3938038
]


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self, n=5):
        i = 0
        target = "0" * n
        ballpark_size = 1e6
        while True:
            i += 1
            AocLogger.log_percent(i, ballpark_size, 10)
            combined = self.text + str(i)
            hex_str = self.md5_hash(combined)
            if hex_str.startswith(target):
                return i

    def p2(self):
        return self.p1(6)

    @classmethod
    def md5_hash(cls, string_to_hash) -> str:
        md5 = hashlib.md5(string_to_hash.encode())
        return md5.hexdigest()

    @classmethod
    def run_tests(cls):
        hex_string = cls.md5_hash('abcdef609043')
        assert hex_string.startswith('00000')


if __name__ == '__main__':
    Solver.run_tests()
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
