#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import itertools

### CONSTANTS ###
TEST_INPUTS = [
    """
20
15
10
5
5
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    4,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    3,
    None,
    None,
]

REAL_OUTPUTS = [
    654,
    None
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.containers = AocUtil.ints(text)
        self.combos = []
        for i in range(2, len(self.containers)):
            self.combos += list(itertools.combinations(self.containers, i))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def get_eggnog_volume(self):
        if self.is_test:
            return 25
        else:
            return 150

    def p1(self):
        result = 0
        for combo in self.combos:
            if sum(combo) == self.get_eggnog_volume():
                AocLogger.log(combo)
                result += 1
        return result

    def p2(self):
        result = 0
        min_size = None
        for combo in self.combos:
            if min_size:
                if len(combo) > min_size:
                    break
            if sum(combo) == self.get_eggnog_volume():
                min_size = len(combo)
                AocLogger.log(combo)
                result += 1
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
