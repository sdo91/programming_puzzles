#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
REAL_OUTPUTS = [
    103,
    405
]

MFCSAM_OUTPUT = """
    children: 3
    cats: 7
    samoyeds: 2
    pomeranians: 3
    akitas: 0
    vizslas: 0
    goldfish: 5
    trees: 3
    cars: 2
    perfumes: 1
"""


class Mfcsam(object):
    def __init__(self, text):
        self.gift_compounds = AocUtil.parse_dict(text, "\n", ":", int)

    def is_match(self, suspect_compounds_text, puzzle_part=1):
        suspect_compounds_dict = AocUtil.parse_dict(suspect_compounds_text, ",", ":", int)
        for key in suspect_compounds_dict:
            machine_reading = self.gift_compounds[key]
            suspect_compound_amount = suspect_compounds_dict[key]

            if puzzle_part == 2 and key in {"cats", "trees"}:
                if suspect_compound_amount <= machine_reading:
                    return False
            elif puzzle_part == 2 and key in {"pomeranians", "goldfish"}:
                if suspect_compound_amount >= machine_reading:
                    return False
            elif suspect_compound_amount != machine_reading:
                return False
        return True


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.machine = Mfcsam(MFCSAM_OUTPUT)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        suspects = []

        for line in self.lines:
            prefix, compounds = line.split(":", 1)

            if self.machine.is_match(compounds, self.part):
                suspects.append(AocUtil.ints(prefix)[0])

        assert len(suspects) == 1
        return suspects[0]

    def p2(self):
        return self.p1()


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    # runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
