#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_logger import AocLogger

### CONSTANTS ###
TEST_INPUTS = [
    """
(())
    """, """
(()(()(
    """, """
)())())
    """
]

TEST_OUTPUTS_P1 = [
    0,
    3,
    -3,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    232,
    1783
]


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        # self.lines = aoc_util.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        for c in self.text:
            if c == '(':
                result += 1
            else:
                result -= 1

        return result

    def p2(self):
        result = 0
        position = 0

        for c in self.text:
            position += 1

            if c == '(':
                result += 1
            else:
                result -= 1

            if result == -1:
                return position


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
