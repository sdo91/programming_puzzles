#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
import itertools

from advent_of_code.util.aoc_logger import AocLogger
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
2x3x4
    """, """
1x1x10
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    58,
    43,
    None,
]

TEST_OUTPUTS_P2 = [
    34,
    14,
    None,
]

REAL_OUTPUTS = [
    1606483,
    3842356
]


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for line in self.lines:
            result += self.calcWrappingPaper(line)
        return result

    def p2(self):
        result = 0
        for line in self.lines:
            result += self.calcRibbon(line)
        return result

    def calcWrappingPaper(self, line):
        dimensions = AocUtil.ints(line)
        combos = list(itertools.combinations(dimensions, 2))

        areas = []
        for pair in combos:
            areas.append(pair[0] * pair[1])
        areas.sort()

        smallest = areas[0]
        result = 2 * sum(areas) + smallest
        return result

    def calcRibbon(self, line):
        dimensions = sorted(AocUtil.ints(line))
        shortest = 2 * (dimensions[0] + dimensions[1])
        bow = dimensions[0] * dimensions[1] * dimensions[2]
        return shortest + bow


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
