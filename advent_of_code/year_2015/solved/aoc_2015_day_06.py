#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import numpy as np

### CONSTANTS ###
TEST_INPUTS = [
    """
turn on 0,0 through 9,9
toggle 0,0 through 9,0
turn off 4,4 through 5,5
    """, """
turn on 0,0 through 0,0
toggle 0,0 through 9,9
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    100 - 10 - 4,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    1 + 200,
    None,
]

REAL_OUTPUTS = [
    543903,
    14687245
]


class LightGrid:
    def __init__(self, size: int):
        # Initialize a grid of specified size with all lights off (False)
        self.grid = np.zeros((size, size), dtype=int)

    def turn_on(self, x1, y1, x2, y2):
        """Turn on lights in the specified rectangular area."""
        self.grid[x1:x2 + 1, y1:y2 + 1] = 1

    def turn_off(self, x1, y1, x2, y2):
        """Turn off lights in the specified rectangular area."""
        self.grid[x1:x2 + 1, y1:y2 + 1] = 0

    def toggle(self, x1, y1, x2, y2):
        """Toggle lights in the specified rectangular area."""
        self.grid[x1:x2 + 1, y1:y2 + 1] += 1
        self.grid[x1:x2 + 1, y1:y2 + 1] %= 2

    def count_lights_on(self):
        """Count the number of lights that are currently on."""
        return np.sum(self.grid)

    def add(self, value, x1, y1, x2, y2):
        self.grid[x1:x2 + 1, y1:y2 + 1] += value

    def floor(self, x1, y1, x2, y2):
        self.grid[x1:x2 + 1, y1:y2 + 1] = np.maximum(0, self.grid[x1:x2 + 1, y1:y2 + 1])


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int, is_test: bool = False):
        self.part = part
        self.text = text
        self.is_test = is_test

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        size = self.get_size()
        light_grid = LightGrid(size)

        for line in self.lines:
            ints = AocUtil.ints(line)
            if 'on' in line:
                light_grid.turn_on(*ints)
            elif 'off' in line:
                light_grid.turn_off(*ints)
            else:
                light_grid.toggle(*ints)

        return light_grid.count_lights_on()

    def p2(self):
        size = self.get_size()
        light_grid = LightGrid(size)

        for line in self.lines:
            ints = AocUtil.ints(line)
            if 'on' in line:
                light_grid.add(1, *ints)
            elif 'off' in line:
                light_grid.add(-1, *ints)
                light_grid.floor(*ints)
            else:
                light_grid.add(2, *ints)

        return light_grid.count_lights_on()

    def get_size(self):
        if self.is_test:
            return 10
        else:
            return 1000


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
