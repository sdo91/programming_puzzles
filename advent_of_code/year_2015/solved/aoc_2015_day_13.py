#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import itertools

### CONSTANTS ###
TEST_INPUTS = [
    """
Alice would gain 54 happiness units by sitting next to Bob.
Alice would lose 79 happiness units by sitting next to Carol.
Alice would lose 2 happiness units by sitting next to David.
Bob would gain 83 happiness units by sitting next to Alice.
Bob would lose 7 happiness units by sitting next to Carol.
Bob would lose 63 happiness units by sitting next to David.
Carol would lose 62 happiness units by sitting next to Alice.
Carol would gain 60 happiness units by sitting next to Bob.
Carol would gain 55 happiness units by sitting next to David.
David would gain 46 happiness units by sitting next to Alice.
David would lose 7 happiness units by sitting next to Bob.
David would gain 41 happiness units by sitting next to Carol.
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    330,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    286,
    None,
    None,
]

REAL_OUTPUTS = [
    709,
    668
]


class Person(object):
    MYSELF = "myself"

    def __init__(self, name):
        self.name = name
        self.scores = {}

    def parse_line(self, line):
        """
        Alice would gain 54 happiness units by sitting next to Bob.
        Alice would lose 79 happiness units by sitting next to Carol.
        """
        line = line.replace(".", "")
        tokens = line.split()
        other_name = tokens[-1]
        score = int(tokens[3])
        if tokens[2] == "lose":
            score *= -1
        self.scores[other_name] = score

    def get_score(self, other_person):
        if self.name == self.MYSELF or other_person.name == self.MYSELF:
            score = 0
        else:
            score = self.scores[other_person.name]
        AocLogger.log("{}: {} from {}".format(self.name, score, other_person.name))
        return score


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text
        self.lines = AocUtil.lines(self.text)

        self.guests = self.parse_input()

        AocLogger.log(str(self))

    def parse_input(self):
        guests = {}
        for line in self.lines:
            # get/create guest
            name = line.split()[0]
            if name not in guests:
                guests[name] = Person(name)
            guest = guests[name]

            # parse the line
            guest.parse_line(line)
        return guests

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        guest_list = list(self.guests.keys())
        first_guest = guest_list[0]
        other_guests = guest_list[1:]

        result = 0
        for permutation in itertools.permutations(other_guests):
            seating_order = [first_guest] + list(permutation)
            score = self.calc_score_p1(seating_order)
            result = max(result, score)

        return result

    def calc_score_p1(self, seating_order):
        AocLogger.log("\nseating_order: {}".format(seating_order))
        score = 0
        for i in range(len(seating_order)):
            j = (i + 1) % len(seating_order)
            left = self.guests[seating_order[i]]
            right = self.guests[seating_order[j]]
            score += left.get_score(right)
            score += right.get_score(left)

        AocLogger.log("score p1: {}".format(score))
        return score

    def p2(self):
        myself = Person(Person.MYSELF)
        self.guests[myself.name] = myself

        return self.p1()


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
