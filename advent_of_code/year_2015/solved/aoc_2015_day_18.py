#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger, AocUtil
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUTS = [
    """
.#.#.#
...##.
#....#
..#...
#.#..#
####..
    """, """
##.#.#
...##.
#....#
..#...
#.#..#
####.#
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    4,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    17,
    None,
]

REAL_OUTPUTS = [
    768,
    781
]


class LifeGrid(Grid2D):
    ALIVE = "#"
    DEAD = "."

    def simulate_generation(self, prev_grid: Grid2D):
        for coord in self.coords():
            new_value = self.ALIVE if self.is_alive(coord, prev_grid) else self.DEAD
            self.set_tuple(coord, new_value)

    def is_alive(self, coord, prev_grid: Grid2D):
        was_alive = prev_grid.is_value(coord, self.ALIVE)
        num_neighbors = prev_grid.count_adjacent(coord, self.ALIVE, include_diagonal=True)
        if was_alive:
            return 2 <= num_neighbors <= 3
        else:
            return num_neighbors == 3

    def count_alive(self):
        return self.count(self.ALIVE)

    def set_corners(self):
        corners = [self.top_left(), self.top_right(), self.bottom_left(), self.bottom_right()]
        for coord in corners:
            self.set_tuple(coord, self.ALIVE)


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.grid = LifeGrid(text)
        if AocLogger.verbose:
            self.grid.show()

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def num_iterations(self):
        if self.is_test:
            return 4 if self.part == 1 else 5
        else:
            return 100

    def p1(self):
        prev_grid = LifeGrid(self.text)
        for step in range(self.num_iterations()):
            AocLogger.log_percent(step, self.num_iterations(), 10)

            # swap grids
            prev_grid, self.grid = self.grid, prev_grid

            # simulate
            self.grid.simulate_generation(prev_grid)
            if self.part == 2:
                self.grid.set_corners()

        return self.grid.count_alive()

    def p2(self):
        return self.p1()


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
