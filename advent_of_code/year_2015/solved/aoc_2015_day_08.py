#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    r'""',
    r'"abc"',
    r'"aaa\"aaa"',
    r'"\x27"',
]

TEST_OUTPUTS_P1 = [
    2,
    2,
    3,
    5,
]

TEST_OUTPUTS_P2 = [
    4,
    4,
    6,
    5,
]

REAL_OUTPUTS = [
    1342,
    2074
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for line in self.lines:
            num_saved = self.process_line_p1(line)
            result += num_saved
        return result

    def p2(self):
        result = 0
        for line in self.lines:
            result += self.process_line_p2(line)
        return result

    def process_line_p1(self, line):
        num_chars_saved = 2
        i = 1
        while i < len(line) - 1:
            if line[i] == '\\':
                next = line[i + 1]
                if next == 'x':
                    num_chars_saved += 3
                    i += 3
                else:
                    num_chars_saved += 1
                    i += 1
            i += 1
        return num_chars_saved

    def process_line_p2(self, line):
        extra_chars = 4
        i = 1
        while i < len(line) - 1:
            if line[i] == '\\' or line[i] == '"':
                extra_chars += 1
            i += 1
        return extra_chars


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
