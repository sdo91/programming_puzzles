#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
    """, """
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
    """, """
Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
    """
]

TEST_OUTPUTS_P1 = [
    1120,
    1056,
    1120,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    689,
]

REAL_OUTPUTS = [
    2640,
    1102
]


class Reindeer(object):
    def __init__(self, text):
        self.name = text.split()[0]
        self.speed, self.flight_time, self.rest_time = AocUtil.ints(text)
        self.score = 0

    def calc_distance(self, time):
        distance = 0

        while time > 0:
            # fly
            time_to_fly = min(time, self.flight_time)
            distance += time_to_fly * self.speed
            time -= time_to_fly

            # rest
            time -= self.rest_time

        return distance


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.racers = self.parse_reindeer()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_reindeer(self):
        result = {}
        for line in self.lines:
            r = Reindeer(line)
            result[r.name] = r
        return result

    def get_race_time(self):
        if self.is_test:
            return 1000
        else:
            return 2503

    def p1(self, t=0):
        best_distance = 0
        p2_leaders = []
        race_time = t if t else self.get_race_time()

        for racer in self.racers.values():
            d = racer.calc_distance(race_time)
            if d > best_distance:
                best_distance = d
                p2_leaders = [racer.name]
            elif d == best_distance:
                p2_leaders.append(racer.name)

        if self.part == 1:
            return best_distance
        else:
            return p2_leaders

    def p2(self):
        # run the races
        for t in range(1, self.get_race_time() + 1):
            leader_names = self.p1(t)
            for name in leader_names:
                racer = self.racers[name]
                racer.score += 1

        # find the winner
        result = 0
        for racer in self.racers.values():
            result = max(result, racer.score)
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
