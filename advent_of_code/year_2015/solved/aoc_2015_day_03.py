#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

from collections import defaultdict

### CONSTANTS ###
TEST_INPUTS = [
    """
>
    """, """
^>v<
    """, """
^v^v^v^v^v
    """
]

TEST_OUTPUTS_P1 = [
    2,
    4,
    2,
]

TEST_OUTPUTS_P2 = [
    None,
    3,
    11,
]

REAL_OUTPUTS = [
    2572,
    2631
]


class HouseAddr(object):
    def __init__(self):
        self.coord = (0, 0)

    def move(self, direction):
        Grid2D.DIRECTIONS = Grid2D.ARROW_DIRECTIONS
        self.coord = Grid2D.get_coord_direction(self.coord, direction)


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        address = HouseAddr()
        houses_visited = defaultdict(int)

        houses_visited[address.coord] += 1

        for c in self.text:
            address.move(c)
            houses_visited[address.coord] += 1

        return len(houses_visited)

    def p2(self):
        santa = HouseAddr()
        robo = HouseAddr()
        houses_visited = {santa.coord}

        santa_turn = True
        for c in self.text:
            if santa_turn:
                santa.move(c)
                houses_visited.add(santa.coord)
            else:
                robo.move(c)
                houses_visited.add(robo.coord)
            santa_turn = not santa_turn

        return len(houses_visited)


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
