#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    "ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb",  # part 1
    "qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy"  # part 2
]

TEST_OUTPUTS_P1 = [
    1,
    1,
    0,
    0,
    0,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
    None,
    None,
    1,
    1,
    0,
    0,
]

REAL_OUTPUTS = [
    238,
    69
]


class Solver(object):

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for line in self.lines:
            if self.is_nice_p1_regex(line):
                result += 1
        return result

    def p2(self):
        result = 0
        for line in self.lines:
            if self.is_nice_p2(line):
                result += 1
        return result

    @staticmethod
    def is_nice_p1(line):
        # It contains at least three vowels
        num_vowels = 0
        for c in line:
            if c in 'aeiou':
                num_vowels += 1
            if num_vowels >= 3:
                break
        if num_vowels < 3:
            return False

        # It contains at least one letter that appears twice in a row
        has_double = False
        for i in range(len(line) - 1):
            if line[i] == line[i + 1]:
                has_double = True
                break
        if not has_double:
            return False

        # It does not contain the strings ab, cd, pq, or xy
        for pair in ["ab", "cd", "pq", "xy"]:
            if pair in line:
                return False
        return True

    @staticmethod
    def is_nice_p1_regex(line):
        # It contains at least three vowels
        if not AocUtil.re_is_match(r"(?:[aeiou].*?){3}", line):
            return False

        # It contains at least one letter that appears twice in a row
        if not AocUtil.re_is_match(r"([a-zA-Z])\1", line):
            return False

        # It does not contain the strings ab, cd, pq, or xy
        return AocUtil.re_is_match(r"^(?!.*(ab|cd|pq|xy))", line)

    @staticmethod
    def is_nice_p2(line):
        # It contains a pair of any two letters that appears at least twice in the string without overlapping
        if not AocUtil.re_is_match(r"([a-zA-Z]{2}).*?\1", line):
            return False

        # It contains at least one letter which repeats with exactly one letter between them
        return AocUtil.re_is_match(r"(\w)\w\1", line)


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
