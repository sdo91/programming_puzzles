#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import itertools

### CONSTANTS ###
TEST_INPUTS = [
    """
London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    605,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    982,
    None,
    None,
]

REAL_OUTPUTS = [
    251,
    898
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.cities = set()
        self.distances = {}

        self.parse_input()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_input(self):
        for line in self.lines:
            # London to Dublin = 464
            a, _, b, _, dist = line.split()
            dist = int(dist)
            self.cities.add(a)
            self.cities.add(b)
            self.distances[(a, b)] = dist
            self.distances[(b, a)] = dist

    def p1(self):
        result = 9e9
        for perm in itertools.permutations(self.cities):
            dist = self.calc_distance(perm)
            result = min(dist, result)
        return result

    def p2(self):
        result = 0
        for perm in itertools.permutations(self.cities):
            dist = self.calc_distance(perm)
            result = max(dist, result)
        return result

    def calc_distance(self, cities):
        dist = 0
        for i in range(len(cities) - 1):
            a, b = cities[i], cities[i + 1]
            dist += self.distances[(a, b)]
        return dist


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
