#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

from collections import deque

### CONSTANTS ###
TEST_INPUTS = [
    """
123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
    """, """
NOT x -> h
NOT y -> i
x AND y -> d
123 -> x
x LSHIFT 2 -> f
x OR y -> e
y RSHIFT 2 -> g
456 -> y
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    65079,
    65079,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    None,
    None,
]

REAL_OUTPUTS = [
    16076,
    2797
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    MAX_UINT16 = 65535

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)
        self.queue = deque(self.lines)

        self.wire_signals = {}

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def get_result_key(self):
        if self.is_test:
            return "i"
        else:
            return "a"

    def p1(self):
        while self.queue:
            line = self.queue.popleft()
            try:
                self.handle_line(line)
            except KeyError:
                self.queue.append(line)  # retry later
        key = self.get_result_key()
        return self.wire_signals[key]

    def p2(self):
        # set b to the result from p1
        self.wire_signals = {'b': self.p1()}
        # reset all other wires (except b!)
        for line in self.lines:
            if not line.endswith("-> b"):
                self.queue.append(line)
        return self.p1()

    def handle_line(self, line):
        lhs, rhs = line.split(" -> ")
        input_values = self.parse_input_values(lhs)
        if 'AND' in lhs:
            self.set_wire(rhs, input_values[0] & input_values[1])
        elif 'OR' in lhs:
            self.set_wire(rhs, input_values[0] | input_values[1])
        elif 'NOT' in lhs:
            self.set_wire(rhs, self.MAX_UINT16 - input_values[0])
        elif 'LSHIFT' in lhs:
            self.set_wire(rhs, input_values[0] << input_values[1])
        elif 'RSHIFT' in lhs:
            self.set_wire(rhs, input_values[0] >> input_values[1])
        else:  # direct input
            self.set_wire(rhs, input_values[0])

    def parse_input_values(self, lhs):
        tokens = lhs.split()
        results = []
        for token in tokens:
            if token.isupper():
                pass  # ignore commands
            elif token.isdigit():
                results.append(int(token))
            else:
                results.append(self.wire_signals[token])  # get from dict
        return results

    def set_wire(self, wire, value):
        self.wire_signals[wire] = value % self.MAX_UINT16


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
