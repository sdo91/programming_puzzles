

# Tips
https://gist.github.com/mcpower/87427528b9ba5cac6f0c679370789661

# Getting inputs
https://github.com/wimglenn/advent-of-code-data

steps:
- copy this link (Ctrl C):
  - https://adventofcode.com/2021/day/10/input
- open new chrome tab (ctrl T)
- open dev console (ctrl shift J)
- click network tab
- nav to aoc input page (Ctrl L, Ctrl V, Enter)
  - see above
- under "Name" click on input
  - other entries might work?
- under "Cookies" look for "session"
- double click "Value", copy
- `echo <session value> > ~/.config/aocd/token`
  - echo abc123 > ~/.config/aocd/token
- NOTE:
  - this works on windows too
  - use git bash
  - you may need: mkdir -p ~/.config/aocd

# PyCharm setup
- mark `programming_puzzles` dir as 'Sources Root' if necessary

# Other setup
- `sudo pip3 install advent-of-code-data`
  - `mkdir -p ~/.config/aocd && touch ~/.config/aocd/token`
  - set cookie as described above
- `sudo pip3 install parse`





