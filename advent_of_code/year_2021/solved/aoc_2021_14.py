#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import defaultdict

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    1588,
    0,
    0,
]

TEST_OUTPUT_2 = [
    2188189693529,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            5656,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            12271437788530,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        half1, half2 = self.text.split('\n\n')

        self.template = half1

        self.lines = aoc_util.lines(half2)
        self.rules = {}
        for line in self.lines:
            tokens = line.split()
            self.rules[tokens[0]] = tokens[-1]

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        polymer = self.template
        for x in range(10):
            polymer = self.apply_step(polymer)

        # count
        counts = aoc_util.count(polymer)
        result = max(counts.values()) - min(counts.values())
        return result

    def apply_step(self, before: str):
        after = [before[0]]
        for x in range(len(before) - 1):
            pair = before[x:x + 2]
            inserted = self.rules[pair]
            after.append(inserted)
            after.append(before[x + 1])
        return ''.join(after)

    def p2(self, num_steps=40):
        """
        p1 too slow...

        algo:
        split into counts of pairs:
        NNCB -> {
            NN: 1
            NC: 1
            CB: 1
        }
        then process each:
        nn -> nc cn
        nc -> nb bc
        cb -> ch hb
        """
        start_polymer = self.template

        # make into dictionary
        pair_counts = defaultdict(int)
        for x in range(len(start_polymer) - 1):
            pair = start_polymer[x:x + 2]
            pair_counts[pair] += 1

        # apply steps
        for step in range(1, num_steps + 1):
            new_counts = defaultdict(int)
            # divide pair into 2 new pairs
            for pair, count in pair_counts.items():
                inserted = self.rules[pair]
                first = pair[0] + inserted
                second = inserted + pair[1]
                new_counts[first] += count
                new_counts[second] += count
            pair_counts = new_counts
            length = sum(pair_counts.values()) + 1
            print('after step {}, len={}'.format(step, length))

        # (double) count elements
        element_counts = defaultdict(int)
        for pair, count in pair_counts.items():
            element_counts[pair[0]] += count
            element_counts[pair[1]] += count

        # the above will double count everything except the first and the last
        # so handle that here
        element_counts[start_polymer[0]] += 1
        element_counts[start_polymer[-1]] += 1
        for element in element_counts.keys():
            element_counts[element] //= 2

        result = max(element_counts.values()) - min(element_counts.values())
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
