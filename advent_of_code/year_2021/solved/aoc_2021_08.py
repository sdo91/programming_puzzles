#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import defaultdict

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
cdfeb fcadb cdfeb cdbaf
    """, """
be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
fgae cfgab fg bagce
    """, """

    """
]

TEST_OUTPUT_1 = [
    0,
    26,
    0,
]

TEST_OUTPUT_2 = [
    5353,
    61229,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            367,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            974512,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):
    segments_by_digit = {
        0: 'abcefg',
        1: 'cf',
        2: 'acdeg',
        3: 'acdfg',
        4: 'bcdf',
        5: 'abdfg',
        6: 'abdefg',
        7: 'acf',
        8: 'abcdefg',
        9: 'abcdfg',
    }

    def __init__(self, text: str):
        self.text = text.strip()

        self.text = self.text.replace('|\n', '| ')

        self.lines = aoc_util.lines(self.text)

        # count how many times each segment is used
        self.segment_counts = self.get_char_counts(self.segments_by_digit.values())

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """
        0: 6
        1: 2*
        2: 5
        3: 5
        4: 4*
        5: 5
        6: 6
        7: 3*
        8: 7*
        9: 6
        """
        counts = defaultdict(int)

        for line in self.lines:
            half1, half2 = aoc_util.split_and_strip_each(line, '|')

            tokens = half2.split()

            for token in tokens:
                cnt = len(token)
                counts[cnt] += 1

        return counts[2] + counts[4] + counts[3] + counts[7]

    def p2(self):
        result = 0

        for line in self.lines:
            half1, half2 = aoc_util.split_and_strip_each(line, '|')

            numbers_by_segments = self.map_segments(half1)

            multiplier = 1000
            for jumble in half2.split():
                sorted_segments = ''.join(sorted(jumble))
                result += multiplier * numbers_by_segments[sorted_segments]
                multiplier //= 10

        return result

    def map_segments(self, text: str):
        """
        result = {
            'bcdef': 5,
            'abcdf': 3,
            etc
        }
        """

        wire_sets_by_len = defaultdict(set)
        for word in text.split():
            wire_set = frozenset(word)
            wire_sets_by_len[len(word)].add(wire_set)

        # decode segments
        wires_by_segment = {}

        # first, use unique counts
        wire_counts_all = self.get_char_counts(text.split())
        for wire, count in wire_counts_all.items():
            if count == 4:
                wires_by_segment['e'] = wire
            elif count == 6:
                wires_by_segment['b'] = wire
            elif count == 9:
                wires_by_segment['f'] = wire

        # A is in 7, but not 1
        wires1 = aoc_util.get_single_value_from_set(wire_sets_by_len[2])
        wires7 = aoc_util.get_single_value_from_set(wire_sets_by_len[3])
        wire_a = aoc_util.get_single_value_from_set(wires7 - wires1)
        wires_by_segment['a'] = wire_a

        # B & D in 4, but not 1
        wires4 = aoc_util.get_single_value_from_set(wire_sets_by_len[4])
        wires_bd = list(wires4 - wires1)
        for wire in wires_bd:
            if wire != wires_by_segment['b']:
                wires_by_segment['d'] = wire

        # finally, look at 5 segment digits
        wire_counts_5 = self.get_char_counts(wire_sets_by_len[5])
        for wire, count in wire_counts_5.items():
            if wire not in wires_by_segment.values():
                if count == 2:
                    # c is the only segment not yet mapped, that appears in 2/3 5 segment digits
                    wires_by_segment['c'] = wire
                elif count == 3:
                    # g is the only segment not yet mapped, that appears in 3/3 5 segment digits
                    wires_by_segment['g'] = wire

        # organize results
        result = {}
        for digit in range(10):
            segments = self.segments_by_digit[digit]
            # collect wires of digit
            wires = [wires_by_segment[s] for s in segments]
            sorted_wires = ''.join(sorted(wires))
            result[sorted_wires] = digit
        return result

    def get_char_counts(self, words):
        char_counts = defaultdict(int)
        for word in words:
            for char in word:
                char_counts[char] += 1
        return char_counts


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
