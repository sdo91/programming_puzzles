#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
16,1,2,0,4,2,7,1,2,14
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    37,
    0,
    0,
]

TEST_OUTPUT_2 = [
    168,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            355989,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            102245489,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.crab_subs = aoc_util.ints(text)

        self.move_costs = [0]

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        min_pos = min(self.crab_subs)
        max_pos = max(self.crab_subs)

        best_cost = 9e9
        for pos in range(min_pos, max_pos + 1):
            cost = 0
            for sub in self.crab_subs:
                cost += abs(sub - pos)
            if AocLogger.verbose:
                print('pos={}, cost={}'.format(pos, cost))
            if cost < best_cost:
                best_cost = cost

        return best_cost

    def p2(self):
        min_pos = min(self.crab_subs)
        max_pos = max(self.crab_subs)

        best_cost = 9e9
        for pos in range(min_pos, max_pos + 1):
            cost = 0
            for sub in self.crab_subs:
                dist = abs(sub - pos)
                cost += self.calc_cost(dist)
            if AocLogger.verbose:
                print('pos={}, cost={}'.format(pos, cost))
            if cost < best_cost:
                best_cost = cost
        return best_cost

    def calc_cost(self, dist):
        while len(self.move_costs) < (dist + 1):
            next_dist = len(self.move_costs)
            next_cost = self.move_costs[-1] + next_dist
            self.move_costs.append(next_cost)
        return self.move_costs[dist]


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
