#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    198,
    0,
    0,
]

TEST_OUTPUT_2 = [
    230,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com

    day 1 has a bunch of review at the beginning
    tips look at input and final question first
    then read only highlighted parts
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            2583164,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            2784375,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        self.lines = aoc_util.lines(text)
        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        num_bits = len(self.lines[0])

        # count bits
        counts0 = [0] * num_bits
        counts1 = [0] * num_bits

        for line in self.lines:
            digits = aoc_util.digits(line)

            for x in range(num_bits):
                if digits[x] == 1:
                    counts1[x] += 1
                else:
                    counts0[x] += 1

        # calculate gamma/epsilon rate
        gamma = 0
        epsilon = 0
        for x in range(num_bits):
            gamma <<= 1
            epsilon <<= 1
            if counts1[x] > counts0[x]:
                gamma |= 0x01
            else:
                epsilon |= 0x01

        return epsilon * gamma

    def p2(self):
        # find ratings
        oxygen = self.find_rating('oxygen')
        co2 = self.find_rating('co2')

        return aoc_util.binary_str_to_int(oxygen) * aoc_util.binary_str_to_int(co2)

    def find_rating(self, type: str):
        # find ox/co2 rating
        num_bits = len(self.lines[0])

        numbers = self.lines.copy()
        for x in range(num_bits):
            most_common = self.find_most_common_bit(numbers, x)
            if most_common == -1:
                most_common = 1

            remaining = []
            for n in numbers:
                digits = aoc_util.digits(n)
                if digits[x] == most_common and type == 'oxygen':
                    remaining.append(n)  # keep most common
                elif digits[x] != most_common and type == 'co2':
                    remaining.append(n)  # keep least common
            numbers = remaining

            if len(numbers) == 1:
                return numbers[0]

    def find_most_common_bit(self, numbers, position):
        count0 = 0
        count1 = 0

        for num in numbers:
            digits = aoc_util.digits(num)

            if digits[position] == 1:
                count1 += 1
            else:
                count0 += 1

        if count0 > count1:
            return 0
        elif count1 > count0:
            return 1
        else:
            return -1  # tie


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
