#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import math
import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
target area: x=20..30, y=-10..-5
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    45,
    0,
    0,
]

TEST_OUTPUT_2 = [
    112,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            10296,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            2371,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.x_min, self.x_max, self.y_min, self.y_max = aoc_util.ints(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        total_max_y = -math.inf

        for x in range(150):
            for y in range(150):
                # trajectory = (7, 2)
                # trajectory = (6, 9)
                trajectory = (x, y)

                is_hit, max_y_pos = self.does_hit_target(trajectory)
                total_max_y = max(max_y_pos, total_max_y)

        return total_max_y

    def p2(self):
        total = 0
        for x in range(150):
            for y in range(-150, 150):
                trajectory = (x, y)

                is_hit, max_y_pos = self.does_hit_target(trajectory)
                if is_hit:
                    total += 1

        return total

    def does_hit_target(self, trajectory):
        pos = 0, 0
        vel = trajectory
        max_y_pos = -math.inf
        while True:
            # move position
            pos = aoc_util.tuple_add(pos, vel)
            max_y_pos = max(max_y_pos, pos[1])

            # check bounds
            if (pos[0] > self.x_max) or (pos[1] < self.y_min):
                return False, 0
            if (self.x_min <= pos[0] <= self.x_max) and (self.y_min <= pos[1] <= self.y_max):
                return True, max_y_pos

            # slow down
            vel = (
                max(0, vel[0] - 1),
                vel[1] - 1
            )


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
