#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import defaultdict

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    17,
    0,
    0,
]

TEST_OUTPUT_2 = [
    0,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            747,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            0,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        half1, half2 = self.text.split('\n\n')

        self.grid = Grid2D(default='.')

        self.dots = set()
        for line in aoc_util.lines(half1):
            coord = tuple(aoc_util.ints(line))
            self.dots.add(coord)
            self.grid.set_tuple(coord, '#')
        if AocLogger.verbose:
            print(self.grid)

        self.folds = aoc_util.lines(half2)

        # AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """

        """
        fold = self.folds[0]
        self.do_fold(fold)
        return len(self.dots)

    def do_fold(self, text: str):
        index = aoc_util.ints(text)[0]
        if 'y=' in text:
            self.fold_y(index)
        else:
            self.fold_x(index)

        if AocLogger.verbose:
            print(self.grid)

    def fold_y(self, index: int):
        new_dots = set()
        for coord in self.dots:
            x, y = coord
            if y > index:
                self.grid.set_tuple(coord, '.')
                new_y = index - (y - index)
                new_coord = (x, new_y)
                self.grid.set_tuple(new_coord, '#')
                new_dots.add(new_coord)
            else:
                new_dots.add(coord)
        self.grid.max_y = index - 1
        self.dots = new_dots

    def fold_x(self, index: int):
        new_dots = set()
        for coord in self.dots:
            x, y = coord
            if x > index:
                self.grid.set_tuple(coord, '.')
                new_x = index - (x - index)
                new_coord = (new_x, y)
                self.grid.set_tuple(new_coord, '#')
                new_dots.add(new_coord)
            else:
                new_dots.add(coord)
        self.grid.max_x = index - 1
        self.dots = new_dots

    def p2(self):
        """
        .##..###..#..#.####.###...##..#..#.#..#.
        #..#.#..#.#..#....#.#..#.#..#.#..#.#..#.
        #..#.#..#.####...#..#..#.#....#..#.####.
        ####.###..#..#..#...###..#....#..#.#..#.
        #..#.#.#..#..#.#....#....#..#.#..#.#..#.
        #..#.#..#.#..#.####.#.....##...##..#..#.

        ARHZPCUH
        """
        for fold in self.folds:
            self.do_fold(fold)
        print(self.grid)
        return 0


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
