#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    26397,
    0,
    0,
]

TEST_OUTPUT_2 = [
    288957,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            367059,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            1952146692,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):
    openers = '([{<'
    closers = ')]}>'

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        for line in self.lines:
            result += self.check_line(line)[0]

        return result

    def check_line(self, line: str):
        stack = []

        for char in line:
            if char in self.closers:
                popped = stack.pop()
                score = self.check_score(popped, char)
                if score:
                    return score, stack
            else:
                # opener
                stack.append(char)
        return 0, stack

    def check_score(self, opener, closer):
        assert opener in self.openers
        if closer == ')' and opener != '(':
            return 3
        elif closer == ']' and opener != '[':
            return 57
        elif closer == '}' and opener != '{':
            return 1197
        elif closer == '>' and opener != '<':
            return 25137
        else:
            return 0

    def p2(self):
        scores = []

        for line in self.lines:
            score, stack = self.check_line(line)
            if score == 0:
                scores.append(self.calc_autocomplete_score(stack))

        scores = list(sorted(scores))

        return scores[len(scores) // 2]

    def calc_autocomplete_score(self, stack):
        score = 0
        for char in reversed(stack):
            score *= 5
            if char == '(':
                score += 1
            if char == '[':
                score += 2
            if char == '{':
                score += 3
            if char == '<':
                score += 4
        return score


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
