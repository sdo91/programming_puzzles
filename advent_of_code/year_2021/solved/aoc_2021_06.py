#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
3,4,3,1,2
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    5934,
    0,
    0,
]

TEST_OUTPUT_2 = [
    26984457539,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            362666,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            1640526601595,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        self.unit_tests()
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def unit_tests(self):
        self.compare_fast_and_slow('0', 0)
        self.compare_fast_and_slow('0', 1)
        self.compare_fast_and_slow('0', 7)
        self.compare_fast_and_slow('0', 8)
        self.compare_fast_and_slow('0', 9)
        self.compare_fast_and_slow('8', 2)
        self.compare_fast_and_slow('8', 9)
        self.compare_fast_and_slow('0', 10)

    def compare_fast_and_slow(self, text: str, num_days):
        solver = Solver(text)
        result_slow = solver.solve_slow(num_days)

        solver = Solver(text)
        result_fast = solver.solve_fast(num_days)

        assert result_slow == result_fast

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.fish_list = aoc_util.ints(text)
        self.cache = {}

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        # return self.solve_slow(80)
        return self.solve_fast(80)

    def solve_slow(self, num_days=80):
        for day in range(1, num_days + 1):
            for x in range(len(self.fish_list)):
                self.fish_list[x] -= 1
                if self.fish_list[x] == -1:
                    self.fish_list.append(8)
                    self.fish_list[x] = 6
            print('after {:2d} days: {}'.format(day, self.fish_list))
        return len(self.fish_list)

    def solve_fast(self, num_days=80):
        result = 0
        for fish in self.fish_list:
            result += self.recursive_fish_calc(fish, num_days)
        return result

    def recursive_fish_calc(self, og_timer, og_num_days_left):
        """
        memoize

        step 1:
            given age, num days, how many times does a given fish reproduce
            (ignore other fish)

            age 0
                0 days: 1 (just the orig)
                1-7 days: 2 ()

        step 2:
            calc when children are born

        recursive_fish_calc(0, 10)
            ans = 4
            1 original fish
            1 child born after 1 day
            1 child born after 8 days
            1 grand born after 10 days

        make a list of children:
            recursive_fish_calc(8, 9) -> 2
            recursive_fish_calc(8, 2) -> 1

        given timer, num_days_left

        while True:
            days_til_next = timer + 1

        after  1 days: [6, 8]
        after  2 days: [5, 7]
        after  3 days: [4, 6]
        after  4 days: [3, 5]
        after  5 days: [2, 4]
        after  6 days: [1, 3]
        after  7 days: [0, 2]
        after  8 days: [6, 1, 8]
        after  9 days: [5, 0, 7]
        after 10 days: [4, 6, 6, 8]

        """
        if AocLogger.verbose:
            print('recursive_fish_calc: timer={}, days left={}'.format(og_timer, og_num_days_left))

        # memoize
        key = (og_timer, og_num_days_left)
        if key in self.cache:
            return self.cache[key]

        # otherwise, calc
        result = 1  # self

        # count children
        timer = og_timer
        num_days_left = og_num_days_left
        while True:
            days_til_next = timer + 1
            num_days_left -= days_til_next
            if num_days_left < 0:
                break

            # figure out how many fish that child will represent
            result += self.recursive_fish_calc(8, num_days_left)

            # reset timer
            timer = 6

        # cache result
        self.cache[key] = result
        if AocLogger.verbose:
            print('result: {} -> {}'.format(key, result))
        return result

    def p2(self):
        return self.solve_fast(256)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
