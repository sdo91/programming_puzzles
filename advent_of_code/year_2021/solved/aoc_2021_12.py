#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
start-A
start-b
A-c
A-b
b-d
A-end
b-end
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    10,
    0,
    0,
]

TEST_OUTPUT_2 = [
    36,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            3369,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            85883,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)

        self.nodes_by_name = self.map_caves(self.lines)

        # p1
        self.all_paths = []

        # p2
        self.current_part = 1

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def map_caves(self, lines):
        nodes_by_name = {}
        for line in lines:
            first, second = line.split('-')
            if first not in nodes_by_name:
                nodes_by_name[first] = Node(first)
            if second not in nodes_by_name:
                nodes_by_name[second] = Node(second)
            nodes_by_name[first].connected_caves.append(second)
            nodes_by_name[second].connected_caves.append(first)
        return nodes_by_name

    def p1(self):
        current_path = []
        self.recursive_get_paths_to_end('start', current_path.copy())
        return len(self.all_paths)

    def recursive_get_paths_to_end(self, cave_name: str, current_path):
        # base cases
        if not self.can_visit_cave(cave_name, current_path):
            return
        if cave_name == 'end':
            current_path.append(cave_name)
            self.all_paths.append(current_path)
            return

        node = self.nodes_by_name[cave_name]
        current_path.append(cave_name)
        for connected in node.connected_caves:
            self.recursive_get_paths_to_end(connected, current_path.copy())

    def can_visit_cave(self, cave_name: str, current_path):
        """
        p2 algo:
        get counts:
        return False if
            start 2+
            end 2+
            any small 3+
            any 2 small 2+
        """
        if self.current_part == 1:
            if cave_name.islower():
                if cave_name in current_path:
                    return False  # already visited
        else:
            # p2
            small_counts = aoc_util.count([cave_name])  # start with the potential new cave
            already_used_extra_visit = False
            for cave in current_path:  # type: str
                if cave.isupper():
                    continue
                small_counts[cave] += 1
                # check we haven't broken the rules
                if small_counts['start'] > 1 or small_counts['end'] > 1:
                    return False
                if small_counts[cave] > 1:
                    if already_used_extra_visit:
                        return False
                    already_used_extra_visit = True
        return True

    def p2(self):
        self.current_part = 2
        current_path = []
        self.recursive_get_paths_to_end('start', current_path.copy())
        return len(self.all_paths)


class Node(object):

    def __init__(self, name: str):
        self.name = name
        self.connected_caves = []

    def __repr__(self):
        return '{}: {}'.format(self.name, self.connected_caves)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
