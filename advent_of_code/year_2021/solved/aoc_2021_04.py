#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    4512,
    0,
    0,
]

TEST_OUTPUT_2 = [
    1924,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            8442,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            4590,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        # self.lines = aoc_util.lines(text)

        tokens = aoc_util.split_and_strip_each(self.text, '\n\n')

        self.numbers = aoc_util.ints(tokens[0])

        self.boards = []

        board_num = 0
        for text in tokens[1:]:
            board_num += 1
            self.boards.append((BingoBoard(text, board_num)))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        for number in self.numbers:
            # number called
            if AocLogger.verbose:
                print('calling {}:'.format(number))

            for board in self.boards:
                is_winner = board.call_number(number)

                if AocLogger.verbose:
                    print(board)

                if is_winner:
                    board.is_winner()
                    return board.calc_score(number)

        assert False

    def p2(self):
        num_boards = len(self.boards)
        num_winners = 0

        for number in self.numbers:
            # number called
            if AocLogger.verbose:
                print('calling {}:'.format(number))
            for board in self.boards:
                if board.has_won:
                    continue  # skip prev winners
                is_winner = board.call_number(number)

                if AocLogger.verbose:
                    print(board)

                if is_winner:
                    num_winners += 1
                    if num_winners == num_boards:
                        return board.calc_score(number)

        assert False


class BingoBoard(object):

    def __init__(self, text: str, board_num: int):
        self.text = text
        self.board_num = board_num

        self.board = []

        self.coords_by_value = {}
        self.value_by_coords = {}

        self.has_won = False

        row = 0
        for line in aoc_util.lines(text):
            numbers = aoc_util.ints(line)
            col = 0
            for value in numbers:
                self.coords_by_value[value] = (row, col)
                self.value_by_coords[(row, col)] = value
                col += 1
            row += 1
            self.board.append([False] * 5)

    def __str__(self):
        builder = ['board: {}\n'.format(self.board_num)]
        for row in range(5):
            for col in range(5):
                value = self.value_by_coords[(row, col)]
                mark = '[x]' if self.board[row][col] else '[ ]'
                square = '{:2d}{}   '.format(value, mark)
                builder.append(square)
            builder.append('\n')
        return ''.join(builder)

    def call_number(self, number: int) -> bool:
        if number not in self.coords_by_value:
            return
        row, col = self.coords_by_value[number]
        self.board[row][col] = True

        return self.is_winner()

    def is_winner(self):
        # check rows
        for row in self.board:
            if all(row):
                self.has_won = True
                return True

        # check cols
        for col in range(5):
            if all(self.board[row][col] for row in range(5)):
                self.has_won = True
                return True

        # check diags (apparently we don't have to do this...)
        # diag1 = []
        # diag2 = []
        # for row in range(5):
        #     col1 = row
        #     col2 = 4 - row
        #     diag1.append(self.board[row][col1])
        #     diag2.append(self.board[row][col2])
        # return all(diag1) or all(diag2)

        return False

    def calc_score(self, number: int) -> int:
        result = 0
        for row in range(5):
            for col in range(5):
                is_marked = self.board[row][col]
                if not is_marked:
                    result += self.value_by_coords[(row, col)]
        return result * number


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
