#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
2199943210
3987894921
9856789892
8767896789
9899965678
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    15,
    0,
    0,
]

TEST_OUTPUT_2 = [
    1134,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            425,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            1135260,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        # aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.grid = Grid2D(text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for row in self.grid.rows():
            for col in self.grid.cols():
                result += self.calc_risk(self.grid, (col, row))
        return result

    @classmethod
    def calc_risk(cls, grid: Grid2D, coord: tuple):
        value = grid.get_tuple(coord)
        for adj_coord in grid.get_adjacent_coords(coord):
            if not grid.has_coord(adj_coord):
                continue
            adj_value = grid.get_tuple(adj_coord)
            if adj_value <= value:
                return 0
        return 1 + int(value)

    def p2(self):
        basin_sizes = []
        for row in self.grid.rows():
            for col in self.grid.cols():
                coord = (col, row)
                is_low_point = self.calc_risk(self.grid, coord) > 0
                if is_low_point:
                    size = self.calc_basin_size(self.grid, coord)
                    basin_sizes.append(size)
        basin_sizes = list(sorted(basin_sizes))
        return basin_sizes[-1] * basin_sizes[-2] * basin_sizes[-3]

    @classmethod
    def calc_basin_size(cls, grid: Grid2D, coord: tuple) -> int:
        basin = Basin()
        basin.recursive_expand(grid, coord)
        return len(basin.coords)


class Basin(object):

    def __init__(self):
        self.coords = set()

    def recursive_expand(self, grid: Grid2D, coord: tuple):
        # base cases
        if not grid.has_coord(coord):
            return
        if grid.is_value(coord, '9'):
            return
        if coord in self.coords:
            return

        # otherwise, coord is part of the basin
        self.coords.add(coord)

        # expand the basin
        for ac in grid.get_adjacent_coords(coord):
            self.recursive_expand(grid, ac)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
