#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import deque

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT_1 = [
    """
D2FE28
    """, """
38006F45291200
    """, """
EE00D40C823060
    """, """
8A004A801A8002F478
    """, """
620080001611562C8802118E34
    """, """
C0015000016115A2E0802F182340
    """, """
A0016C880162017C3686B18A3D4780
    """
]

TEST_OUTPUT_1 = [
    6,
    9,
    14,
    16,
    12,
    23,
    31,
]

TEST_INPUT_2 = [
    """
C200B40A82
    """, """
04005AC33890
    """, """
880086C3E88112
    """, """
CE00C43D881120
    """, """
D8005AC2A8F0
    """, """
F600BC2D8F
    """, """
9C005AC2F8F0
    """, """
9C0141080250320F1802104A08
    """
]

TEST_OUTPUT_2 = [
    3,
    54,
    7,
    9,
    1,
    0,
    0,
    1,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            955,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            158135423448,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT_1, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT_2, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        builder = []
        for char in self.text:
            builder.append(self.parse_hex(char))
        self.binary_string = ''.join(builder)

        self.bits = deque()
        for char in self.binary_string:
            self.bits.append(char)

        # p1
        self.version_sum = 0

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_hex(self, char):
        as_int = int(char, 16)
        result = aoc_util.int_to_binary_str(as_int)
        while len(result) < 4:
            result = '0' + result
        return result

    def p1(self):
        while True:
            self.read_next_packet()
            if len(self.bits) < 11:
                break
        return self.version_sum

    def read_next_packet(self):
        version = self.read_bits_int(3)
        self.version_sum += version

        type_id = self.read_bits_int(3)

        if type_id == 4:
            # literal
            literal = self.read_literal()
            if AocLogger.verbose:
                print('literal: {}'.format(literal))
            return literal
        # otherwise we got an operator

        # parse the args
        length_type_id = self.read_bits_int(1)
        if length_type_id:
            args = self.read_subpackets_1()
        else:
            args = self.read_subpackets_0()

        # calc the result
        if type_id == 0:
            return sum(args)
        if type_id == 1:
            product = 1
            for x in args:
                product *= x
            return product
        if type_id == 2:
            return min(args)
        if type_id == 3:
            return max(args)

        assert len(args) == 2
        if type_id == 5:
            return int(args[0] > args[1])
        if type_id == 6:
            return int(args[0] < args[1])
        if type_id == 7:
            return int(args[0] == args[1])

        assert False  # should not get to here

    def read_bits_str(self, num_bits):
        builder = []
        for x in range(num_bits):
            builder.append(self.bits.popleft())
        return ''.join(builder)

    def read_bits_int(self, num_bits):
        return int(self.read_bits_str(num_bits), 2)

    def read_literal(self):
        builder = []
        while True:
            group = self.read_bits_str(5)
            builder.append(group[1:])
            if group[0] == '0':
                break
        binary = ''.join(builder)
        return int(binary, 2)

    def read_subpackets_1(self):
        # number of sub-packets immediately contained
        num_packets = self.read_bits_int(11)
        result = []
        for x in range(num_packets):
            result.append(self.read_next_packet())
        return result

    def read_subpackets_0(self):
        # read total length in bits

        # calc end condition
        num_bits = self.read_bits_int(15)
        current_bits_len = len(self.bits)
        goal_len = current_bits_len - num_bits

        # read the packets
        result = []
        while True:
            result.append(self.read_next_packet())
            if len(self.bits) == goal_len:
                return result
            if len(self.bits) < goal_len:
                assert False

    def p2(self):
        result = self.read_next_packet()
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
