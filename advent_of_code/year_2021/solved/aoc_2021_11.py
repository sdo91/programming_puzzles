#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    1656,
    0,
    0,
]

TEST_OUTPUT_2 = [
    195,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
            # self.puzzle_input = open('11.in').read()
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            1700,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            273,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.lines = aoc_util.lines(self.text)
        self.grid = Grid2D(text)
        self.total_flashes = 0

        # AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        """

        """
        self.total_flashes = 0
        for step in range(1, 101):
            self.do_step(step)
        return self.total_flashes

    def do_step(self, step):
        for row in self.grid.rows():
            for col in self.grid.cols():
                coord = (col, row)
                self.recursive_inc(coord)

        # reset
        for row in self.grid.rows():
            for col in self.grid.cols():
                coord = (col, row)
                if self.grid.is_value(coord, '*'):
                    self.grid.set_tuple(coord, 0)

        if AocLogger.verbose and step:
            if step < 10 or step % 10 == 0:
                print('\nafter step {} (total_flashes={})'.format(step, self.total_flashes))
                print(self.grid)

    def recursive_inc(self, coord: tuple):
        if not self.grid.has_coord(coord):
            return  # OOB

        prev = self.grid.get_tuple(coord)
        if prev == '*':
            return  # already flashed
        else:
            prev = int(prev)

        new_value = prev + 1
        if new_value > 9:
            # flash
            self.grid.set_tuple(coord, '*')
            self.total_flashes += 1
            for adj in self.grid.get_adjacent_coords(coord, True):
                self.recursive_inc(adj)
        else:
            # just inc
            self.grid.set_tuple(coord, new_value)

    def p2(self):
        self.total_flashes = 0

        step = 0
        while True:
            step += 1
            prev = self.total_flashes
            self.do_step(step=None)
            if (self.total_flashes - prev) == 100:
                return step


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
