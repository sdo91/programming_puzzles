#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import defaultdict

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger

### CONSTANTS ###
TEST_INPUT = [
    """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    5,
    0,
    0,
]

TEST_OUTPUT_2 = [
    12,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com

          -------Part 1--------   -------Part 2--------
    Day       Time  Rank  Score       Time  Rank  Score
      5   00:09:33   572      0   00:19:04   842      0
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            5294,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            21698,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()
        self.lines = aoc_util.lines(text)
        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        return self.count_overlaps(include_diagonals=False)

    def p2(self):
        return self.count_overlaps(include_diagonals=True)

    def count_overlaps(self, include_diagonals):
        counts = defaultdict(int)

        for line in self.lines:
            # parse the line
            x1, y1, x2, y2 = aoc_util.ints(line)

            # calc x/y deltas
            dx = 0
            if x2 < x1:
                dx = -1
            elif x2 > x1:
                dx = 1

            dy = 0
            if y2 < y1:
                dy = -1
            elif y2 > y1:
                dy = 1

            if not include_diagonals and (dx != 0 and dy != 0):
                continue  # skip diag

            # process each coord in the line
            line_length = max(abs(x1 - x2), abs(y1 - y2)) + 1
            for offset in range(line_length):
                coord = x1 + (offset * dx), y1 + (offset * dy)
                counts[coord] += 1

        # sum multi-overlaps
        result = 0
        for coord in counts:
            if counts[coord] > 1:
                result += 1
        return result


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
