#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.dijkstra_solver import DijkstraSolver
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    40,
    0,
    0,
]

TEST_OUTPUT_2 = [
    315,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            583,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            2927,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        self.grid = Grid2D(self.text)

        # p2
        assert self.grid.max_x == self.grid.max_y
        self.input_size = self.grid.max_x + 1
        print('input_size: {}'.format(self.input_size))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        dijkstra_solver = ChitonSolver(self.grid)
        final_node = dijkstra_solver.find_shortest_path()
        return final_node.dist

    def p2(self):
        """
        algo:
        for each coord in small grid
            process coord

        process algo:
        take (0,0) = 1
        generate list of 25 coords
            (x + (tile_x), y = tile_y)

        for each big coord:
            calc manhattan dist
            increment value that many times
        """
        original_grid = self.grid
        expanded_grid = Grid2D()

        for og_coord in original_grid.coords():
            # coord in the original grid
            original_value = int(original_grid.get_tuple(og_coord))

            # expand 5x in each direction
            expanded_coords = self.generate_expanded_coords(og_coord)

            # print('original_value: {}'.format(original_value))
            for ex_coord in expanded_coords:
                manhattan_dist = self.get_manhattan_dist(ex_coord)
                new_value = self.increment_risk(original_value, manhattan_dist)
                # print('ex_coord={}, manhattan_dist={}, new_value={}'.format(ex_coord, manhattan_dist, new_value))
                expanded_grid.set_tuple(ex_coord, str(new_value))

        if AocLogger.verbose:
            print(expanded_grid)

        dijkstra_solver = ChitonSolver(expanded_grid)
        final_node = dijkstra_solver.find_shortest_path()
        return final_node.dist

    def generate_expanded_coords(self, original_coord):
        expanded_coords = []
        for tile_y in range(5):
            for tile_x in range(5):
                expanded_x = (tile_x * self.input_size) + original_coord[0]
                expanded_y = (tile_y * self.input_size) + original_coord[1]
                expanded_coords.append((expanded_x, expanded_y))
        return expanded_coords

    def get_manhattan_dist(self, expanded_coord):
        tile_x = expanded_coord[0] // self.input_size
        tile_y = expanded_coord[1] // self.input_size
        return aoc_util.manhatten_dist((tile_x, tile_y))

    def increment_risk(self, original_value, num_times):
        result = original_value
        for x in range(num_times):
            result += 1
            if result > 9:
                result = 1
        return result


class ChitonSolver(DijkstraSolver):

    def __init__(self, grid: Grid2D):
        open_chars = set([char for char in '987654321'])
        goal_chars = set()
        super().__init__(grid, open_chars, goal_chars)

        self.goal_coord = (grid.max_x, grid.max_y)

    def is_done(self, coord):
        return coord == self.goal_coord

    def distance(self, first, second):
        risk = self.grid.get_tuple(second)
        return int(risk)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
