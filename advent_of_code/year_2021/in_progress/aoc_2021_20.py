#!/usr/bin/env python3

def addToPath(relPath):
    from os import path
    import sys
    dirOfThisFile = path.dirname(path.realpath(__file__))
    dirToAdd = path.normpath(path.join(dirOfThisFile, relPath))
    if dirToAdd not in sys.path:
        print('adding to path: {}'.format(dirToAdd))
        sys.path.insert(0, dirToAdd)
    else:
        print('already in path: {}'.format(dirToAdd))


addToPath('../../..')

### IMPORTS ###

import time
import traceback
from collections import defaultdict

import aocd

from advent_of_code.util import aoc_util
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUT = [
    """
..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##
#..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###
.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.
.#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....
.#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..
...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....
..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###
    """, """

    """, """

    """
]

TEST_OUTPUT_1 = [
    35,
    0,
    0,
]

TEST_OUTPUT_2 = [
    3351,
    0,
    0,
]


class AdventOfCode(object):
    """
    https://adventofcode.com
    """

    def __init__(self):
        print('starting {}'.format(__file__.split('/')[-1]))

        try:
            self.puzzle_input = aocd.data
        except aocd.exceptions.AocdError:
            error_msg = traceback.format_exc()
            print(error_msg)
            self.puzzle_input = 'unable to get input:\n\n{}'.format(error_msg)
        aoc_util.write_input(self.puzzle_input, __file__)

    def run(self):
        start_time = time.time()

        self.run_tests()

        AocLogger.verbose = False

        aoc_util.assert_equal(
            5231,
            self.solve_part_1(self.puzzle_input)
        )

        aoc_util.assert_equal(
            14279,
            self.solve_part_2(self.puzzle_input)
        )

        elapsed_time = time.time() - start_time
        print('elapsed_time: {:.3f} sec'.format(elapsed_time))

    def run_tests(self):
        AocLogger.verbose = True
        aoc_util.run_tests(self.solve_part_1, TEST_INPUT, TEST_OUTPUT_1)
        aoc_util.run_tests(self.solve_part_2, TEST_INPUT, TEST_OUTPUT_2)

    def solve_part_1(self, text: str):
        solver = Solver(text)

        part_1_result = solver.p1()

        print('part_1_result: {}\n'.format(part_1_result))
        return part_1_result

    def solve_part_2(self, text: str):
        solver = Solver(text)

        part_2_result = solver.p2()

        print('part_2_result: {}\n'.format(part_2_result))
        return part_2_result


class Solver(object):

    def __init__(self, text: str):
        self.text = text.strip()

        tokens = self.text.split('\n\n')

        self.algo = tokens[0].replace('\n', '')
        assert len(self.algo) == 2 ** 9

        self.image = Grid2D(tokens[1], default='.')
        print(self.image)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self, iterations=2):
        """
        5405 is too high
        """
        buffer = 4
        for i in range(iterations):
            if i % 5 == 0:
                print('i={}'.format(i))
            enhanced = Grid2D(default='.')

            top_left = Grid2D.adjust_coord(self.image.top_left(), -buffer, -buffer)
            bot_right = Grid2D.adjust_coord(self.image.bottom_right(), buffer, buffer)
            coords = Grid2D.get_block(top_left, bot_right)

            for coord in coords:
                new_value = self.enhance_pixel(coord)
                enhanced.set_tuple(coord, new_value)

            self.image = enhanced
            if AocLogger.verbose:
                print(self.image)

            if aoc_util.is_odd(i):
                self.trim_edges(buffer + 2)

        print('final image:')
        print(self.image)
        return self.image.count('#')

    def enhance_pixel(self, coord):
        # get block of 9 coords
        top_left = Grid2D.get_coord_nw(coord)
        bot_right = Grid2D.get_coord_se(coord)
        coords = Grid2D.get_block(top_left, bot_right)
        assert len(coords) == 9

        binary_num = 0
        for pixel_coord in coords:
            value = self.image.get_tuple(pixel_coord)
            # print(value)

            binary_num <<= 1
            if value == '#':
                binary_num |= 0x01

        enhanced_value = self.algo[binary_num]
        return enhanced_value

    def trim_edges(self, buffer):
        top_left = Grid2D.adjust_coord(self.image.top_left(), buffer, buffer)
        bot_right = Grid2D.adjust_coord(self.image.bottom_right(), -buffer, -buffer)
        coords = Grid2D.get_block(top_left, bot_right)

        trimmed = Grid2D(default='.')
        for coord in coords:
            value = self.image.get_tuple(coord)
            trimmed.set_tuple(coord, value)

        self.image = trimmed

    def p2(self):
        AocLogger.verbose = False
        return self.p1(iterations=50)


if __name__ == '__main__':
    instance = AdventOfCode()
    instance.run()
