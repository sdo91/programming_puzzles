#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

from collections import defaultdict

### CONSTANTS ###
TEST_INPUTS = [
    """
3   4
4   3
2   5
1   3
3   9
3   3
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    11,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    31,
    None,
    None,
]

REAL_OUTPUTS = [
    3714264,
    18805872
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        left = []
        right = []
        for line in self.lines:
            a, b = AocUtil.ints(line)
            left.append(a)
            right.append(b)

        left = sorted(left)
        right = sorted(right)

        distances = []
        for i in range(len(left)):
            d = abs(left[i] - right[i])
            distances.append(d)

        return sum(distances)

    def p2(self):
        left = []
        right = defaultdict(int)
        for i in AocUtil.range_len(self.lines):
            a, b = AocUtil.ints(self.lines[i])
            left.append(a)
            right[b] += 1

        result = 0
        for n in left:
            score = n * right[n]
            result += score
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
