#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUTS = [
    """
..X...
.SAMX.
.A..A.
XMAS.S
.X....
    """, """
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
    """, """
M.S
.A.
M.S
    """
]

TEST_OUTPUTS_P1 = [
    4,
    18,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    9,
    1,
]

REAL_OUTPUTS = [
    2571,
    1992
]


class XmasGrid(Grid2D):
    def count_xmas(self):
        matches = 0
        for coord in self.coords():
            if self.is_value(coord, "X"):
                matches += self.count_from(coord)
        return matches

    def count_from(self, coord):
        matches = 0
        vectors = self.get_adjacent_deltas(True)
        for vector in vectors:
            if self.is_match(coord, vector):
                matches += 1
        return matches

    def is_match(self, coord, vector):
        target_word = "XMAS"
        current_coord = coord
        for x in range(len(target_word)):
            if not self.is_value(current_coord, target_word[x]):
                return False
            current_coord = self.adjust_coord(current_coord, *vector)
        return True

    def count_xmas_p2(self):
        matches = 0
        for coord in self.coords():
            if not self.is_value(coord, "A"):
                continue
            # check fox X-MAS
            if self.is_match_p2(coord):
                matches += 1
        return matches

    def is_match_p2(self, coord):
        if not self.is_ms_pair(coord, self.DELTA_NW, self.DELTA_SE):
            return False
        return self.is_ms_pair(coord, self.DELTA_NE, self.DELTA_SW)

    def is_ms_pair(self, coord, delta1, delta2):
        values = set()
        for d in [delta1, delta2]:
            adjusted_coord = self.adjust_coord(coord, *d)
            values.add(self.get(*adjusted_coord))
        return len(values) == 2 and "M" in values and "S" in values


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.grid = XmasGrid(text)
        self.grid.show()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = self.grid.count_xmas()
        return result

    def p2(self):
        result = self.grid.count_xmas_p2()
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
