#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

import itertools

### CONSTANTS ###
TEST_INPUTS = [
    """
190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    3749,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    11387,
    None,
    None,
]

REAL_OUTPUTS = [
    465126289353,
    70597497486371
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        i = 0
        for line in self.lines:
            if self.part == 2 and not AocLogger.verbose:
                AocLogger.log_percent(i, self.lines, 10)
            result += self.process_line(line)
            i += 1
        return result

    def p2(self):
        return self.p1()

    def process_line(self, line):
        ints = AocUtil.ints(line)
        test_value = ints[0]
        result_so_far = ints[1]
        numbers = ints[2:]

        num_ops = len(numbers)

        combos = self.get_operators(num_ops)

        for combo in combos:
            if self.is_valid_combo(test_value, result_so_far, numbers, combo):
                return test_value
        return 0

    def get_operators(self, n):
        s = "*+"
        if self.part == 2:
            s += "|"
        return itertools.product(s, repeat=n)

    @staticmethod
    def is_valid_combo(test_value, result_so_far, numbers, operators):
        assert len(numbers) == len(operators)
        for i in range(len(numbers)):
            if operators[i] == "*":
                result_so_far *= numbers[i]
            elif operators[i] == "+":
                result_so_far += numbers[i]
            elif operators[i] == "|":
                concat = "{}{}".format(result_so_far, numbers[i])
                result_so_far = int(concat)
            else:
                assert False
        return result_so_far == test_value


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
