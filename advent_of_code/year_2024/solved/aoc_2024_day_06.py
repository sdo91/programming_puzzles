#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil
from advent_of_code.util.grid_2d import Grid2D

### CONSTANTS ###
TEST_INPUTS = [
    """
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    41,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    6,
    None,
    None,
]

REAL_OUTPUTS = [
    4776,
    1586
]


class GuardSim(Grid2D):
    FACING_DIRECTIONS = "^>v<"

    OBSTRUCTIONS = "#O"

    def __init__(self, text):
        super().__init__(text=text, default=".", overlay_chars=self.FACING_DIRECTIONS)
        self.start_pos = list(self.overlay.keys())[0]
        self.start_arrow = self.get_top(self.start_pos)

        self.turns = set()

    def reset(self):
        self.overlay.clear()
        self.overlay[self.start_pos] = self.start_arrow

        self.turns = set()

    def run_sim(self):
        # setup
        distinct_positions = {self.start_pos}

        # run the simulation
        current_pos = self.start_pos
        while True:
            # get facing coord
            facing_coord = self.get_facing_coord(current_pos)

            # check if p1 done
            if self.is_out_of_bounds(facing_coord):
                break  # done!

            # check for obstacle
            if self.get_top(facing_coord) in self.OBSTRUCTIONS:
                if self.is_loop_detected(current_pos):
                    return -1
                self.rotate(current_pos)
                continue

            # update overlay
            arrow_char = self.get_top(current_pos)
            self.overlay[current_pos] = "X"
            self.overlay[facing_coord] = arrow_char

            # step forward
            current_pos = facing_coord
            distinct_positions.add(current_pos)

        # clean up
        self.overlay[current_pos] = "X"
        self.overlay[self.start_pos] = self.start_arrow
        return len(distinct_positions)

    def get_facing_coord(self, current_pos):
        arrow_char = self.get_top(current_pos)
        facing_delta = self.ARROW_DIRECTIONS[arrow_char]
        facing_coord = self.adjust_coord(current_pos, *facing_delta)
        return facing_coord

    def rotate(self, current_pos):
        arrow_char = self.get_top(current_pos)
        next_index = (self.FACING_DIRECTIONS.find(arrow_char) + 1) % 4
        next_arrow = self.FACING_DIRECTIONS[next_index]
        self.overlay[current_pos] = next_arrow

    def p2(self):
        # run p1
        self.run_sim()

        # find all Xs
        x_positions = self.find_all_in_overlay("X")

        # count loops
        result = 0
        i = 0
        for obstruction_pos in x_positions:
            if not AocLogger.verbose:
                AocLogger.log_percent(i, x_positions, 10)
            if self.does_obstruction_create_loop(obstruction_pos):
                result += 1
            i += 1
        return result

    def does_obstruction_create_loop(self, obstruction_pos):
        # reset the map
        self.reset()

        # place the obstruction
        self.overlay[obstruction_pos] = "O"
        # self.show()

        # run the sim
        is_loop_detected = (self.run_sim() < 0)
        return is_loop_detected

    def is_loop_detected(self, current_pos):
        arrow_char = self.get_top(current_pos)
        turn = "{}@{}".format(arrow_char, current_pos)
        if turn in self.turns:
            return True
        else:
            self.turns.add(turn)
            return False


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.grid = GuardSim(self.text)
        # self.grid.show()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = self.grid.run_sim()
        self.grid.show()
        return result

    def p2(self):
        result = self.grid.p2()
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
