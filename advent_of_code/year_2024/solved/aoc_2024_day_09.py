#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

from collections import defaultdict

### CONSTANTS ###
TEST_INPUTS = [
    """
12345
    """, """
2333133121414131402
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    60,
    1928,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    2858,
    None,
]

REAL_OUTPUTS = [
    6242766523059,
    6272188244509,
]


class Block(object):
    def __init__(self, file_id, chunk_id=0, file_size=0):
        self.file_id = file_id
        self.chunk_id = chunk_id
        self.file_size = file_size

    def __repr__(self):
        if self.is_empty():
            return "."
        return "file_id={}, chunk={}/{}".format(
            self.file_id, self.chunk_id + 1, self.file_size)

    @classmethod
    def create_empty(cls):
        return Block(-1)

    def is_empty(self):
        return self.file_id < 0

    def calc_file_start_pos(self, pos):
        result = pos - self.chunk_id
        return result


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        self.blocks = self.parse_input()
        print("disk_size: {}".format(len(self.blocks)))

        self.insert_pos = 0

        self.file_insert_positions_by_size = defaultdict(int)
        self.moved_files = set()

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def parse_input(self):
        is_file = True
        file_id = 0
        blocks = []
        for digit_str in self.text:
            file_size = int(digit_str)
            for chunk_id in range(file_size):
                if is_file:
                    block = Block(file_id, chunk_id, file_size)
                    blocks.append(block)
                else:
                    blocks.append(Block.create_empty())
            if is_file:
                file_id += 1
            is_file = not is_file
        return blocks

    def p1(self):
        self.move_blocks()
        result = self.calc_checksum()
        return result

    def move_blocks(self):
        """
        The amphipod would like to move file blocks one at a time
        from the end of the disk
        to the leftmost free space block
        (until there are no gaps remaining between file blocks).
        """
        start_pos = len(self.blocks) - 1
        for i in range(start_pos, 0, -1):
            block = self.blocks[i]
            if block.is_empty():
                continue
            is_done = not self.insert_block(block, i)
            if is_done:
                return
            self.blocks[i] = Block.create_empty()

    def insert_block(self, block: Block, old_pos):
        for new_pos in range(self.insert_pos, len(self.blocks)):
            if new_pos >= old_pos:
                return False
            slot = self.blocks[new_pos]
            if slot.is_empty():
                self.blocks[new_pos] = block
                self.insert_pos = new_pos
                return True
        return False

    def calc_checksum(self):
        result = 0
        for pos in range(len(self.blocks)):
            block = self.blocks[pos]
            if block.is_empty():
                continue
            score = pos * block.file_id
            result += score
            # AocLogger.log("add pos={} * id={}".format(pos, block.file_id))
        return result

    def p2(self):
        self.move_files()
        result = self.calc_checksum()
        return result

    def move_files(self):
        old_pos = len(self.blocks)
        while True:
            old_pos -= 1

            AocLogger.log_percent(old_pos, self.blocks)

            # check if done
            if old_pos <= 0:
                return  # done!

            block = self.blocks[old_pos]
            if block.is_empty():
                continue

            # we now have a valid file
            # get file start position
            file_start_pos = block.calc_file_start_pos(old_pos)

            # check if already moved
            file_id = block.file_id
            is_already_moved = (file_id in self.moved_files)

            # attempt to move the file
            if is_already_moved:
                AocLogger.log("  skipped file {}".format(file_id))
            else:
                file_blocks = self.blocks[file_start_pos:old_pos + 1]
                success = self.insert_file(file_blocks, file_start_pos)

                # handle result
                if success:
                    # track moved files
                    self.moved_files.add(file_id)
                    # clear blocks
                    for x in range(len(file_blocks)):
                        pos_to_clear = file_start_pos + x
                        self.blocks[pos_to_clear] = Block.create_empty()
                    AocLogger.log("file {} moved".format(file_id))
                else:
                    AocLogger.log("file {} not moved".format(file_id))

            # prep for next loop
            old_pos = file_start_pos

    def insert_file(self, file_blocks, old_file_start_pos):
        file_size = len(file_blocks)
        new_pos = self.file_insert_positions_by_size[file_size]
        while True:
            new_pos += 1
            if new_pos >= old_file_start_pos:
                return False  # file not moved

            # scan until we find empty space
            slot = self.blocks[new_pos]
            if not slot.is_empty():
                continue

            # check if there is enough space
            free_space = self.calc_free_space(new_pos)
            if free_space >= file_size:
                # move it here!
                new_file_start = new_pos
                for x in range(file_size):
                    new_pos = new_file_start + x
                    self.blocks[new_pos] = file_blocks[x]
                return True
            new_pos += free_space

    def calc_free_space(self, start_pos):
        x = start_pos
        while True:
            slot = self.blocks[x]
            if slot.is_empty():
                x += 1
            else:
                # end of free space
                space = x - start_pos
                return space


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    # runner.skip_p1_tests()
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
