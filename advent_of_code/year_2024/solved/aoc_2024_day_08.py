#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.grid_2d import Grid2D

from collections import defaultdict
import itertools

### CONSTANTS ###
TEST_INPUTS = [
    """
..........
..........
..........
....a.....
........a.
.....a....
..........
..........
..........
..........
    """, """
............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............
    """, """
T.........
...T......
.T........
..........
..........
..........
..........
..........
..........
..........
    """
]

TEST_OUTPUTS_P1 = [
    4,
    14,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    34,
    9,
]

REAL_OUTPUTS = [
    423,
    1287
]


class AntennaMap(Grid2D):
    def __init__(self, text):
        super().__init__(text, default=".")

        self.antennas_dict = self.get_antennas()

    def get_antennas(self):
        result = defaultdict(set)
        for coord in self.coords():
            value = self.get(coord)
            if value == ".":
                continue
            result[value].add(coord)
        return result

    def count_antinodes(self, part):
        for frequency in self.antennas_dict:
            self.place_antinodes(frequency, part)

        self.show()

        return len(self.overlay)

    def place_antinodes(self, frequency, part):
        coords = self.antennas_dict[frequency]
        for pair in itertools.combinations(coords, 2):
            antinodes = self.get_antinode_coords(pair, part)
            for an_coord in antinodes:
                if self.is_in_bounds(an_coord):
                    self.overlay[an_coord] = "#"

    def get_antinode_coords(self, pair, part):
        a, b = pair
        a_to_b = self.subtract(b, a)
        b_to_a = self.subtract(a, b)
        antinode_coords = set()
        if part == 1:
            antinode_coords.add(self.adjust_coord(b, *a_to_b))
            antinode_coords.add(self.adjust_coord(a, *b_to_a))
        else:  # part 2
            current_coord = b
            while self.is_in_bounds(current_coord):
                antinode_coords.add(current_coord)
                current_coord = self.adjust_coord(current_coord, *a_to_b)
            current_coord = a
            while self.is_in_bounds(current_coord):
                antinode_coords.add(current_coord)
                current_coord = self.adjust_coord(current_coord, *b_to_a)
        return antinode_coords


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.grid = AntennaMap(text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = self.grid.count_antinodes(1)
        return result

    def p2(self):
        result = self.grid.count_antinodes(2)
        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
