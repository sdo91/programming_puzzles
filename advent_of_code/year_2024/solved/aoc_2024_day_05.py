#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    143,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    123,
    None,
    None,
]

REAL_OUTPUTS = [
    4662,
    5900
]


class PageOrderRule(object):
    def __init__(self, page_number):
        self.page_number = page_number
        self.prequels = set()
        self.sequels = set()

    def __repr__(self):
        return "[PageOrderRule: {} {} {} front_size={}]".format(
            self.prequels, self.page_number, self.sequels, len(self.prequels))


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.halves = text.split("\n\n")

        self.rule_lines = AocUtil.lines(self.halves[0])
        self.rules = {}
        for line in self.rule_lines:
            before, after = AocUtil.ints(line)
            if before not in self.rules:
                self.rules[before] = PageOrderRule(before)
            self.rules[before].sequels.add(after)
            if after not in self.rules:
                self.rules[after] = PageOrderRule(after)
            self.rules[after].prequels.add(before)

        self.update_lines = AocUtil.lines(self.halves[1])

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0
        for line in self.update_lines:
            score = self.check_order(line)
            result += score
        return result

    def check_order(self, line):
        pages = AocUtil.ints(line)
        front = set()
        back = set(pages)
        assert len(pages) == len(back)  # assumption
        for page in pages:
            back.remove(page)
            page_rules = self.rules[page]

            broken_rules = page_rules.prequels & back
            if broken_rules:
                # broken rule
                AocLogger.log("pages {} must be before {}".format(broken_rules, page))
                return 0

            broken_rules = page_rules.sequels & front
            if broken_rules:
                # broken rule
                AocLogger.log("pages {} must be after {}".format(broken_rules, page))
                return 0

            front.add(page)

        # return middle
        middle = len(pages) // 2
        return pages[middle]

    def p2(self):
        result = 0
        for line in self.update_lines:
            score = self.check_order(line)
            if score == 0:
                # fix incorrectly-ordered update
                fixed_line = self.fix_order(line)
                score = self.check_order(fixed_line)
                assert score > 0
                result += score
        return result

    def fix_order(self, line):
        result_order = []
        page_numbers = AocUtil.ints(line)
        for page in page_numbers:
            result_order = self.insert(result_order, page)
        return ",".join(map(str, result_order))

    def insert(self, result_order, new_page):
        new_page_rules = self.rules[new_page]
        for x in range(len(result_order)):
            # check if new_page needs to go here
            current_page = result_order[x]
            if current_page in new_page_rules.sequels:
                # found position
                new_order = result_order[:x] + [new_page] + result_order[x:]
                return new_order
        result_order.append(new_page)  # if we get to the end, append
        return result_order


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
