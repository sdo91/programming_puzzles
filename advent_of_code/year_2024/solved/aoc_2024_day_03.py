#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
    """, """
xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
    """, """

    """
]

TEST_OUTPUTS_P1 = [
    161,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    None,
    48,
    None,
]

REAL_OUTPUTS = [
    182780583,
    90772405
]


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        result = 0

        matches = AocUtil.re_find_all_matches(r"mul\([0-9]+,[0-9]+\)", self.text)
        for match in matches:
            numbers = AocUtil.ints(match)
            result += numbers[0] * numbers[1]

        return result

    def p2(self):
        result = 0

        is_enabled = True

        matches = AocUtil.re_find_all_matches(r"mul\([0-9]+,[0-9]+\)|do\(\)|don't\(\)", self.text)
        for match in matches:
            if "mul" in match:
                if is_enabled:
                    numbers = AocUtil.ints(match)
                    result += numbers[0] * numbers[1]
            else:
                is_enabled = (match == "do()")

        return result


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
