#!/usr/bin/env python3

def add_to_path(rel_path):
    from os import path
    import sys
    dir_of_this_file = path.dirname(path.realpath(__file__))
    dir_to_add = path.normpath(path.join(dir_of_this_file, rel_path))
    if dir_to_add not in sys.path:
        print('adding to path: {}'.format(dir_to_add))
        sys.path.insert(0, dir_to_add)
    else:
        print('already in path: {}'.format(dir_to_add))


add_to_path('../../..')

### IMPORTS ###
from advent_of_code.util.aoc_runner import AocRunner
from advent_of_code.util.aoc_util import AocLogger
from advent_of_code.util.aoc_util import AocUtil

### CONSTANTS ###
TEST_INPUTS = [
    """
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
    """, """

    """, """

    """
]

TEST_OUTPUTS_P1 = [
    2,
    None,
    None,
]

TEST_OUTPUTS_P2 = [
    4,
    None,
    None,
]

REAL_OUTPUTS = [
    299,
    364
]


class Report(object):
    def __init__(self, text):
        self.text = text

        self.levels = AocUtil.ints(text)

        self.is_increasing = True
        self.min_delta = 1
        self.max_delta = 3
        self.calibrate_limits()

    def __repr__(self):
        return self.text

    def calibrate_limits(self):
        self.is_increasing = self.levels[1] > self.levels[0]
        if self.is_increasing:
            self.min_delta = 1
            self.max_delta = 3
        else:
            self.min_delta = -3
            self.max_delta = -1

    def reload(self, index_to_skip):
        self.levels = AocUtil.ints(self.text)
        del self.levels[index_to_skip]
        self.calibrate_limits()

    def is_safe(self):
        return self.find_problem() < 0

    def find_problem(self):
        for i in range(len(self.levels) - 1):
            delta = self.levels[i + 1] - self.levels[i]
            if not (self.min_delta <= delta <= self.max_delta):
                return i + 1
        return -1

    def is_safe_p2(self):
        if self.is_safe():
            return True
        for i in AocUtil.range_len(self.levels):
            self.reload(index_to_skip=i)
            if self.is_safe():
                return True
        return False


class Solver(object):
    # to be set by AocRunner
    is_test = False

    def __init__(self, text: str, part: int):
        self.part = part
        self.text = text

        self.lines = AocUtil.lines(self.text)
        self.reports = []
        for line in self.lines:
            self.reports.append(Report(line))

        AocLogger.log(str(self))

    def __repr__(self):
        return '{}:\n{}\n'.format(
            type(self).__name__, self.text)

    def p1(self):
        num_safe = 0
        for report in self.reports:
            if report.is_safe():
                num_safe += 1
        return num_safe

    def p2(self):
        num_safe = 0
        for report in self.reports:
            if report.is_safe_p2():
                num_safe += 1
        return num_safe


if __name__ == '__main__':
    runner = AocRunner(Solver, __file__)
    runner.run_tests(TEST_INPUTS, TEST_OUTPUTS_P1, TEST_OUTPUTS_P2)
    runner.run(REAL_OUTPUTS)
